package com.kamakhya.dibzi.setting;

public class AppConstance {

    public static final String LOGIN_TYPE_EMAIL = "email";
    public static final String DEVICE_TYPE = "android";
    //INSPECTION TYPE
    public static final int INCLINE_INCEPTION=1;
    public static final int GOLD_SEAL_SAMPLE=2;
    public static final int WEEKLY_SAFETY=3;
    public static final int PILOT_PRODUCTION_CHECKLIST=4;
    public static final int PRE_FINAL_INSPECTION=5;
    //API TYPE
    public static final int SCHEDULE_BY_SHIPMENT=1;
    public static final int VIEW_ALL_SHIPMENT=0;
    public static final int VIEW_SHIPMENT_CUSTOM=2;
    //API ENDPOINTS && BASE URL
    public static final String BASE_URL="http://sandboxapi.dizbi.com/api/";
    public static final String AUTHORIZE_TOKEN="Authorization-Token";
    public static final String SLNO="slno";
    public static final String LOGIN="auth/Login";
    public static final String CREATE_SCHEDULE="Inspection/CreateSchedule";
    public static final String CREATE_SCHEDULE_BY_SHIPMENT="Inspection/CreateScheduleByShipment";
    public static final String GET_PO_GARMENTS="Inspection/GetPONumbers";
    public static final String GET_PO_BY_SHIPMENT_ID="Inspection/GetPODetailsByShipmentId";
    public static final String GET_PO_GARMENTS_DETAIL="Inspection/GetPODetails";
    public static final String GET_SCHEDULE_BY_SHIPMENT="Inspection/GetSchedules";
    public static final String GET_SCHEDULE_BY_DATE="Inspection/GetSchedules";
    public static final String DELETE_SCHEDULE_BY_SHIPMENT="Inspection/DeleteScheduleByShipment";
    public static final String DELETE_SCHEDULE="Inspection/DeleteSchedule";
    public static final String AUDITORS="Inspection/GetAuditors";
    public static final String AUDIT_LIST="Inspection/GetAuditTypes";
    public static final String SHIPMENT_IDS="Inspection/GetShipmentIds";
    public static final String UPDATE_DATA_SHIPMENT_IDS="Inspection/GetScheduleByShipment";
}
