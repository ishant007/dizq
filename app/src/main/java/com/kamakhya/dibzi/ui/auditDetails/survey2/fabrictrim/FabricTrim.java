package com.kamakhya.dibzi.ui.auditDetails.survey2.fabrictrim;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.CommentDilaogLayBinding;
import com.kamakhya.dibzi.databinding.FragmentFabricTrimBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class FabricTrim extends BaseFragment<FragmentFabricTrimBinding,AuditDetailViewmodel> {
    private static FabricTrim instance;
    private FragmentFabricTrimBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Dialog commentDialog;
    private CommentDilaogLayBinding bindingDialog;

    public FabricTrim() {
        // Required empty public constructor
    }

    @Override
    public String toString() {
        return FabricTrim.class.getName();
    }

    public static FabricTrim newInstance() {
        instance= instance==null? new FabricTrim():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fabric_trim;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.comment_dilaog_lay, null, false);
        GetCommentDialog();
        DesigningSetup();
        clickListners();
    }

    private void DesigningSetup() {
        binding.buttonLay.HeaderName.setText(getResources().getString(R.string.button));
        binding.pinchSettingLay.HeaderName.setText(getResources().getString(R.string.pinch_setting));
        binding.snapsLay.HeaderName.setText(getResources().getString(R.string.sanps_poppers));
        binding.threadLay.HeaderName.setText(getResources().getString(R.string.thread));
        //correctBtn
        binding.buttonLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.pinchSettingLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.snapsLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.threadLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.zipperLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        //incorreect
        binding.buttonLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.pinchSettingLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.snapsLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.threadLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.zipperLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        //not acceptable
        binding.buttonLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.pinchSettingLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.snapsLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.threadLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.zipperLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
    }

    private void clickListners() {
        binding.zipperLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();

            }
        });
        binding.threadLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.snapsLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.pinchSettingLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.buttonLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private Dialog GetCommentDialog() {
        if (commentDialog == null) {
            commentDialog = new Dialog(getContext());
            commentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commentDialog.setContentView(bindingDialog.getRoot());
            final Window window = commentDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            bindingDialog.close.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
            bindingDialog.cancel.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
        }
        return commentDialog;
    }
}