package com.kamakhya.dibzi.ui.auditDetails.survey5.compliancecheckedbox;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentComplianceCheckedBoxBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.adapter.GarmentAdapter;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.modelclass.GarmentItem;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class ComplianceCheckedBoxFragment extends BaseFragment<FragmentComplianceCheckedBoxBinding,AuditDetailViewmodel> {
private static ComplianceCheckedBoxFragment instance;
private AuditDetailViewmodel viewmodel;
private FragmentComplianceCheckedBoxBinding binding;

    public ComplianceCheckedBoxFragment() {
        // Required empty public constructor
    }


    public static ComplianceCheckedBoxFragment newInstance() {
        instance= instance ==null? new ComplianceCheckedBoxFragment():instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_compliance_checked_box;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}