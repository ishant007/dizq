package com.kamakhya.dibzi.ui.auditDetails.survey3.productsaftey;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentProductSafetyBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey3.productsaftey.adapter.QuestionAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class ProductSafetyFragment extends BaseFragment<FragmentProductSafetyBinding,AuditDetailViewmodel> {
   private static ProductSafetyFragment instance;
   private FragmentProductSafetyBinding binding;
   private AuditDetailViewmodel viewmodel;
   private QuestionAdapter questionAdapter;

    public ProductSafetyFragment() {
        // Required empty public constructor
    }


    public static ProductSafetyFragment newInstance() {
        instance= instance== null? new ProductSafetyFragment():instance;
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return ProductSafetyFragment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_product_safety;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        questionAdapter=new QuestionAdapter(getContext());
        binding.questionRecycler.setAdapter(questionAdapter);
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomAlert("Question Submited Succ");
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}