package com.kamakhya.dibzi.ui.auditDetails.survey3.healthandsaftey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentProductSafetyBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey3.productsaftey.adapter.QuestionAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class HealthAndSafetyFragment extends BaseFragment<FragmentProductSafetyBinding,AuditDetailViewmodel> {
   private static HealthAndSafetyFragment instance;
   private FragmentProductSafetyBinding binding;
   private AuditDetailViewmodel viewmodel;
   private QuestionAdapter questionAdapter;

    public HealthAndSafetyFragment() {
        // Required empty public constructor
    }


    public static HealthAndSafetyFragment newInstance() {
        instance= instance== null? new HealthAndSafetyFragment():instance;
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return getResources().getString(R.string.health_and_safety);
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_product_safety;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        questionAdapter=new QuestionAdapter(getContext());
        binding.questionRecycler.setAdapter(questionAdapter);
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomAlert("Question Submited Succ");
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}