package com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.retrofitModel.getauditlist.ResultsItem;

import java.util.ArrayList;
import java.util.List;

public class AuditListAdapter extends ArrayAdapter<ResultsItem> {
    private List<ResultsItem> objects = new ArrayList<>();
    private  LayoutInflater mInflater;
    private  Context mContext;
    private  int mResource;


    public AuditListAdapter(Context context, int resource, List<ResultsItem> objects) {
        super(context, resource, objects);
        this.objects = objects;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        CheckedTextView spinneText = view.findViewById(R.id.textSpinner);
        spinneText.setText(objects.get(position).getText());
        return view;
    }
}
