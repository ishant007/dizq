package com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementreport;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentMeasurementReportBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementreport.adapter.MesurementReportAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class MeasurementReportFragment extends BaseFragment<FragmentMeasurementReportBinding, AuditDetailViewmodel> {

    private static MeasurementReportFragment instance;
    private MesurementReportAdapter adapter;
    private FragmentMeasurementReportBinding binding;
    private AuditDetailViewmodel viewmodel;

    public MeasurementReportFragment() {
        // Required empty public constructor
    }

    @Override
    public String toString() {
        return MeasurementReportFragment.class.getName();
    }

    public static MeasurementReportFragment newInstance() {
        instance = instance == null ? new MeasurementReportFragment() : instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_measurement_report;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter = new MesurementReportAdapter(getContext());
        binding.mesurementReportRecycler.setAdapter(adapter);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}