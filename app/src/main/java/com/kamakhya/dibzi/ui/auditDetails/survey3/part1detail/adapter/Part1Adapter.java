package com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.Part1DetailItemBinding;
import com.kamakhya.dibzi.databinding.SurveyQuestionLayBinding;

public class Part1Adapter extends RecyclerView.Adapter<Part1Adapter.Part1DetailViewHolder> {
    private Context context;
    private int type;

    public Part1Adapter(Context context,int type) {
        this.context = context;
        this.type=type;
    }

    @NonNull
    @Override
    public Part1DetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Part1DetailItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.part1_detail_item, parent, false);
        return new Part1DetailViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull Part1DetailViewHolder holder, int position) {
        holder.onBindView(position);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class Part1DetailViewHolder extends RecyclerView.ViewHolder {
        private Part1DetailItemBinding binding;

        public Part1DetailViewHolder(@NonNull Part1DetailItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onBindView(int position) {
            if(type==1){
                binding.failBtn.setText("NO");
                binding.passBtn.setText("YES");
            }else if(type==2){
                binding.failBtn.setText("NO");
                binding.passBtn.setText("YES");
                binding.addcomment.setVisibility(View.VISIBLE);
                binding.addPicture.setVisibility(View.VISIBLE);
            }else if(type==3){
                binding.addcomment.setVisibility(View.VISIBLE);
            }
            else if(type==4){
                binding.addcomment.setVisibility(View.VISIBLE);
            }
        }
    }
}
