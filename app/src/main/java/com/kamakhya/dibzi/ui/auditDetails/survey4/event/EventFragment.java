package com.kamakhya.dibzi.ui.auditDetails.survey4.event;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentEventBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class EventFragment extends BaseFragment<FragmentEventBinding,AuditDetailViewmodel> {
    private static EventFragment fragment;
    private FragmentEventBinding binding;
    private AuditDetailViewmodel viewmodel;

    public EventFragment() {
        // Required empty public constructor
    }

    public static EventFragment newInstance() {
        fragment = fragment == null ? new EventFragment() : fragment;
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_event;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=viewmodel==null?new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity()):viewmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.washingStanderdLay.questionsTxt.setText(R.string.washing_standard);
        binding.WashingStandLay.questionsTxt.setText(R.string.washing_stand);
        binding.PressingMethodLightLay.questionsTxt.setText(R.string.press_light);
        binding.PressingMethodHardLay.questionsTxt.setText(R.string.press_hard);
        binding.FoldingLay.questionsTxt.setText(R.string.folding_method);
        binding.PressingMethodLightLay.notApplicableBtn.setText(R.string.not_received);
        binding.PressingMethodHardLay.notApplicableBtn.setText(R.string.not_received);
        binding.FoldingLay.notApplicableBtn.setText(R.string.not_received);


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}