package com.kamakhya.dibzi.ui.schedules;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.dibzi.baseclasses.BaseViewModel;
import com.kamakhya.dibzi.prefrences.SharedPre;
import com.kamakhya.dibzi.retrofitModel.deleteSchedule.DeleteScheduleResponse;
import com.kamakhya.dibzi.retrofitModel.getPoDetails.GetPoDetailsResponse;
import com.kamakhya.dibzi.retrofitModel.getSchedule.GetScheduleResponse;
import com.kamakhya.dibzi.setting.AppConstance;

import java.util.ArrayList;

public class ScheduleViewModel extends BaseViewModel<ScheduleNav> {
    public ScheduleViewModel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }

    public void GetShcheduleByShipment(String shipmentId) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL + AppConstance.GET_SCHEDULE_BY_SHIPMENT)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN, getSharedPre().getUserToken())
                .addQueryParameter("poSlNo", shipmentId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetScheduleResponse.class, new ParsedRequestListener<GetScheduleResponse>() {
                    @Override
                    public void onResponse(GetScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getResults() != null && response.getResults().size() > 0) {
                            getNavigator().GetSchedules(response.getResults());
                        } else {
                            getNavigator().showMessage("No Schedules Found");
                            getNavigator().GetSchedules(new ArrayList<>());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void DeleteShchedule(String slno,int pos) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL + AppConstance.DELETE_SCHEDULE)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN, getSharedPre().getUserToken())
                .addQueryParameter("slno", slno)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(DeleteScheduleResponse.class, new ParsedRequestListener<DeleteScheduleResponse>() {
                    @Override
                    public void onResponse(DeleteScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getResults() != null && response.getResults().getReturnStatus().equalsIgnoreCase("Success")) {
                            getNavigator().DeleteSchedule(response.getResults().getReturnMessage(),pos);
                        } else {
                            getNavigator().Error("Schedule Not Deleted Successfully");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void DELETE_SCHEDULE_BY_SHIPMENT(String slno,int pos) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL + AppConstance.DELETE_SCHEDULE_BY_SHIPMENT)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN, getSharedPre().getUserToken())
                .addQueryParameter("slno", slno)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(DeleteScheduleResponse.class, new ParsedRequestListener<DeleteScheduleResponse>() {
                    @Override
                    public void onResponse(DeleteScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getResults() != null && response.getResults().getReturnStatus().equalsIgnoreCase("Success")) {
                            getNavigator().DeleteSchedule(response.getResults().getReturnMessage(),pos);
                        } else {
                            getNavigator().Error("Schedule Not Deleted Successfully");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void GetShcheduleByDate(String date,String month,String year) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL + AppConstance.GET_SCHEDULE_BY_DATE)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN, getSharedPre().getUserToken())
                .addQueryParameter("date", date)
                .addQueryParameter("month", month)
                .addQueryParameter("year", year)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetScheduleResponse.class, new ParsedRequestListener<GetScheduleResponse>() {
                    @Override
                    public void onResponse(GetScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getResults() != null && response.getResults().size() > 0) {
                            getNavigator().GetSchedules(response.getResults());
                        } else {
                            getNavigator().showMessage("No Schedules Found");
                            getNavigator().GetSchedules(new ArrayList<>());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void GetShchedule() {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL + AppConstance.GET_SCHEDULE_BY_DATE)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN, getSharedPre().getUserToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetScheduleResponse.class, new ParsedRequestListener<GetScheduleResponse>() {
                    @Override
                    public void onResponse(GetScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getResults() != null && response.getResults().size() > 0) {
                            getNavigator().GetSchedules(response.getResults());
                        } else {
                            getNavigator().showMessage("No Schedules Found");
                            getNavigator().GetSchedules(new ArrayList<>());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
}
