package com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentComplianceBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.adapter.QuestionAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

public class Presentation extends BaseFragment<FragmentComplianceBinding, AuditDetailViewmodel> {
    private static Presentation instance;
    private FragmentComplianceBinding binding;
    private AuditDetailViewmodel viewmodel;
    private QuestionAdapter questionAdapter;

    public Presentation() {
        // Required empty public constructor
    }

    public static Presentation newInstance() {
        instance = instance == null ? new Presentation() : instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_compliance;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        questionAdapter = new QuestionAdapter(getContext());
        binding.questionRecycler.setAdapter(questionAdapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}