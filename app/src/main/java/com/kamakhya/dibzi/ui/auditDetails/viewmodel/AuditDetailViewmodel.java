package com.kamakhya.dibzi.ui.auditDetails.viewmodel;

import android.app.Activity;
import android.content.Context;

import com.kamakhya.dibzi.baseclasses.BaseViewModel;
import com.kamakhya.dibzi.prefrences.SharedPre;

public class AuditDetailViewmodel extends BaseViewModel<AuditDetailNav> {
    public AuditDetailViewmodel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }
}
