package com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentProductPictureBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.adapter.GarmentAdapter;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.modelclass.GarmentItem;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class ProductPicture extends BaseFragment<FragmentProductPictureBinding,AuditDetailViewmodel> {
    private FragmentProductPictureBinding binding ;
    private AuditDetailViewmodel viewmodel;
    private ProductPicture instance;
    private GarmentAdapter adapter;


    public ProductPicture() {
        // Required empty public constructor
    }


    public static ProductPicture newInstance() {
        ProductPicture fragment = new ProductPicture();
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_product_picture;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new GarmentAdapter(getContext());
        binding.AddGarmentsRecycler.setAdapter(adapter);
        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GarmentItem garmentItem=new GarmentItem(0,"");
                adapter.UpdateList(garmentItem);

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}