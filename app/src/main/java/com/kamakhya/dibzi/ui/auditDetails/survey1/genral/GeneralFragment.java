package com.kamakhya.dibzi.ui.auditDetails.survey1.genral;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentGeneralBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import static com.kamakhya.dibzi.setting.AppConstance.WEEKLY_SAFETY;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GeneralFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralFragment extends BaseFragment<FragmentGeneralBinding,AuditDetailViewmodel> {
    private FragmentGeneralBinding binding;
    private AuditDetailViewmodel viewmodel;
    private static GeneralFragment instance;
    private Dialog internetDialog;
    private static int pageVisible=1;



    public GeneralFragment() {
        // Required empty public constructor
    }

    @Override
    public String toString() {
        return GeneralFragment.class.getName();
    }

    public static GeneralFragment newInstance(int pageVisibleMain) {
        pageVisible=pageVisibleMain;
        instance= instance == null?new GeneralFragment():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_general;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        switch (pageVisible){
            case WEEKLY_SAFETY:{
                binding.lay2.setVisibility(View.GONE);
                binding.lay3.setVisibility(View.GONE);
                binding.lay4.setVisibility(View.GONE);
                binding.lay5.setVisibility(View.GONE);
                binding.lay6.setVisibility(View.GONE);
                break;
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}