package com.kamakhya.dibzi.ui.setting.fragment.menu;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.BuildConfig;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentSettingBinding;
import com.kamakhya.dibzi.setting.AppConstance;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.login.Login;
import com.kamakhya.dibzi.ui.schedules.SchedulesActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingNav;
import com.kamakhya.dibzi.ui.setting.activity.SettingViewmodel;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.InspectionsFragment;
import com.kamakhya.dibzi.ui.setting.fragment.scheduleshipment.ScheduleShipment;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;

public class SettingFragment extends BaseFragment<FragmentSettingBinding, SettingViewmodel>  {
    private static SettingFragment instance;
    private FragmentSettingBinding binding;
    private SettingViewmodel viewmodel;
    private Disposable disposable;
    private Dialog internetDialog;


    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        instance = instance == null ? new SettingFragment() : instance;
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return SettingFragment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    public SettingViewmodel getViewModel() {
        return viewmodel = new SettingViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getViewDataBinding() != null) {
            binding = getViewDataBinding();
            binding.name.setText(getSharedPre().getUserRole()+" : "+getSharedPre().getName());
            binding.versionName.setText("Version : "+BuildConfig.VERSION_NAME);
           if(getSharedPre().getEmailProfile()!=null && !getSharedPre().getEmailProfile().isEmpty()){
               Glide.with(this).load(getSharedPre().getEmailProfile()).placeholder(R.drawable.round_placeholder).into(binding.profileImage);
           }else {
               Glide.with(this).load(R.drawable.round_placeholder).into(binding.profileImage);
           }

            ((SettingActivity) getBaseActivity()).getToolbar().title.setText(getString(R.string.app_name));
            onClickListeners();
        }
    }

    private void onClickListeners() {
        disposable = RxView.clicks(binding.schduleRequestBtn).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                getBaseActivity().startFragment(InspectionsFragment.newInstance("","0"), true, InspectionsFragment.newInstance("","0").toString());
            }
        });
        disposable = RxView.clicks(binding.schdueleShipment).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                getBaseActivity().startFragment(ScheduleShipment.newInstance(), true, ScheduleShipment.newInstance().toString());
            }
        });
        disposable = RxView.clicks(binding.viewAllSchedule).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                    startActivity(new Intent(getBaseActivity(), SchedulesActivity.class).putExtra("type", AppConstance.VIEW_ALL_SHIPMENT));
            }
        });
        disposable = RxView.clicks(binding.deleteSchedule).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
               // viewmodel.DeleteAllSchedule();

            }
        });
        disposable = RxView.clicks(binding.logout).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                startActivity(new Intent(getBaseActivity(), Login.class));
                getSharedPre().Logout();
                getBaseActivity().finish();
            }
        });
        disposable = RxView.clicks(binding.profileLay).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}