package com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementdiscri.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.DeleteDialogLayBinding;
import com.kamakhya.dibzi.databinding.EditDialogLayBinding;
import com.kamakhya.dibzi.databinding.FindingAndCommentItemBinding;

public class MesurementDiscrepanciesAdapter extends RecyclerView.Adapter<MesurementDiscrepanciesAdapter.MesurementDiscrepanciesViewHolder> {
    private Context context;
    private Dialog editDialog, deleteDialog;
    private EditDialogLayBinding editDialogLayBinding;
    private DeleteDialogLayBinding deleteDialogLayBinding;

    public MesurementDiscrepanciesAdapter(Context context) {
        this.context = context;
    }

    public void UpdateList() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MesurementDiscrepanciesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FindingAndCommentItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.finding_and_comment_item, parent, false);
        return new MesurementDiscrepanciesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MesurementDiscrepanciesViewHolder holder, int position) {
        holder.ViewHolderConnection(position);
    }

    private Dialog GetEditDialog() {
        if (editDialog == null) {
            editDialogLayBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.edit_dialog_lay, null, false);
            editDialog = new Dialog(context);
            editDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            editDialog.setContentView(editDialogLayBinding.getRoot());
            final Window window = editDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            editDialogLayBinding.back.setOnClickListener(v -> {
                editDialogLayBinding.addCommentTxt.setText("");
                editDialog.dismiss();
            });
        }
        return editDialog;
    }

    public Dialog GetDeleteDialog() {
        if (deleteDialog == null) {
            deleteDialogLayBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.delete_dialog_lay, null, false);
            deleteDialog = new Dialog(context);
            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            deleteDialog.setContentView(deleteDialogLayBinding.getRoot());
            final Window window = deleteDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            deleteDialogLayBinding.close.setOnClickListener(v -> {
                deleteDialog.dismiss();
            });
            deleteDialogLayBinding.cancel.setOnClickListener(v -> {
                deleteDialog.dismiss();
            });
        }

        return deleteDialog;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class MesurementDiscrepanciesViewHolder extends RecyclerView.ViewHolder {
        private FindingAndCommentItemBinding binding;

        public MesurementDiscrepanciesViewHolder(@NonNull FindingAndCommentItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void ViewHolderConnection(final int position) {
            binding.commentTxt.setText("Demo Comment");

            binding.showImg.setOnClickListener(v -> {
                GetEditDialog().show();
                editDialogLayBinding.addCommentTxt.setText(binding.commentTxt.getText().toString());

            });
            binding.delete.setOnClickListener(v -> {
                GetDeleteDialog().show();
            });
        }

    }
}
