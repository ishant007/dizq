package com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.QuestionComplianceItemBinding;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionComplianceViewHolder> {
    private Context context;

    public QuestionAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public QuestionComplianceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        QuestionComplianceItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.question_compliance_item,parent,false);
        return new QuestionComplianceViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionComplianceViewHolder holder, final int position) {
        holder.onBindView(position);

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class QuestionComplianceViewHolder extends RecyclerView.ViewHolder {
        private QuestionComplianceItemBinding binding;
        public QuestionComplianceViewHolder(@NonNull QuestionComplianceItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void onBindView(final int position) {
        }
    }
}
