package com.kamakhya.dibzi.ui.login;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kamakhya.dibzi.baseclasses.BaseViewModel;
import com.kamakhya.dibzi.prefrences.SharedPre;
import com.kamakhya.dibzi.retrofitModel.login.Response;
import com.kamakhya.dibzi.setting.AppConstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.TypeVariable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class LoginViewModel extends BaseViewModel<LoginNav> {
    private String IpAddress;
    public LoginViewModel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
        IpAddress=getLocalIpAddress();
    }
    public void Login(String username ,String password){
        getNavigator().showLoading(true);
        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("UserName", username);
            jsonObject.put("Password", password);
            if(IpAddress!=null){
                jsonObject.put("IPAddress", IpAddress);
            }else{
                jsonObject.put("IPAddress", "192.168.1.1");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppConstance.BASE_URL+AppConstance.LOGIN)
                .addJSONObjectBody(jsonObject) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(Response.class, new ParsedRequestListener<Response>() {
                    @Override
                    public void onResponse(Response response) {
                        getNavigator().showLoading(false);
                        if(response!=null &&response.getResults().getLoginStatus()!=null && response.getResults().getLoginStatus().equalsIgnoreCase("Success")){
                           getSharedPre().setUserId(String.valueOf(response.getResults().getUserId()));
                            getSharedPre().setIsEmailLoggedIn(true);
                           getSharedPre().setUserEmail(response.getResults().getUserEmail());
                           getSharedPre().setName(response.getResults().getEmployeeName());
                           getSharedPre().setEmailProfile(response.getResults().getUserImage());
                           getSharedPre().setUSerToken(response.getResults().getToken());
                           getSharedPre().setUserRole(response.getResults().getUserRole());
                           String json=new Gson().toJson(response, new TypeToken<Response>(){}.getType());
                           getSharedPre().setUserResponse(json);
                           /* Response response2=new Gson().fromJson(getSharedPre().getUserResponse(), new TypeToken<Response>() {
                            }.getType());*/
                           getNavigator().startHome();
                        }else{
                            getNavigator().showMessage("Invalid Email or Password !!");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
    public  String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        IpAddress=inetAddress.getHostAddress();
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
