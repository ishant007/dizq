package com.kamakhya.dibzi.ui.auditDetails.survey4.aprovedtrimfabric;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentApprovedFabricTrimBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class ApprovedFabricTrim extends BaseFragment<FragmentApprovedFabricTrimBinding,AuditDetailViewmodel> {
    private static ApprovedFabricTrim fragment;
    private FragmentApprovedFabricTrimBinding binding;
    private AuditDetailViewmodel viewmodel;


    public ApprovedFabricTrim() {
        // Required empty public constructor
    }


    public static ApprovedFabricTrim newInstance() {
        fragment= fragment==null?new ApprovedFabricTrim():fragment;
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_approved_fabric_trim;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=viewmodel==null?new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity()):viewmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.fabricLay.questionsTxt.setText(R.string.fabric);
        binding.buttonLay.questionsTxt.setText(R.string.button);
        binding.buttonLay.notApplicableBtn.setVisibility(View.VISIBLE);
        binding.interliningLay.questionsTxt.setText(R.string.inter_lining);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}