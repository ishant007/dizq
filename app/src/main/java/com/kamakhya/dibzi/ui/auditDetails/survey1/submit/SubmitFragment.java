package com.kamakhya.dibzi.ui.auditDetails.survey1.submit;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.EmailLayBinding;
import com.kamakhya.dibzi.databinding.FragmentSubmitBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubmitFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubmitFragment extends BaseFragment<FragmentSubmitBinding, AuditDetailViewmodel> {

    private static SubmitFragment instance;
    private AuditDetailViewmodel viewmodel;
    private FragmentSubmitBinding binding;
    private EmailLayBinding emailBinding;
    private int index=4;

    public SubmitFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }

    public static SubmitFragment newInstance() {
        return instance =instance==null? new SubmitFragment():instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_submit;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        emailBinding= DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.email_lay, null, false);
        binding.submitSign.setText("SIGN & SUBMIT");
        binding.signTxt.setText("SIGN & SUBMIT");
        clickListners();
    }

    private void clickListners() {
        EditText text1 = new EditText(getContext());
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp1.setMargins(0, 10, 0, 10);
        text1.setBackgroundResource(R.drawable.edit_text_back);
        text1.setHint(getContext().getResources().getString(R.string.email_id));
        text1.setLayoutParams(lp1);
        binding.emailContainer.addView(text1);
        EditText text2 = new EditText(getContext());
        text2.setBackgroundResource(R.drawable.edit_text_back);
        text2.setHint(getContext().getResources().getString(R.string.email_id));
        text2.setLayoutParams(lp1);
        binding.emailContainer.addView(text2);
        EditText text3 = new EditText(getContext());
        text3.setBackgroundResource(R.drawable.edit_text_back);
        text3.setHint(getContext().getResources().getString(R.string.email_id));
        text3.setLayoutParams(lp1);
        binding.emailContainer.addView(text3);
        binding.submitSign.setOnClickListener(v -> {
            if(binding.checkTerms.isChecked()){
                binding.lay1.setVisibility(View.GONE);
                binding.lay2.setVisibility(View.VISIBLE);
            }else{
                showCustomAlert("Please Check terms & Condition");
            }

        });
        binding.clearAuditor.setOnClickListener(v -> binding.signatureAuditor.clearCanvas());
        binding.clearVendore.setOnClickListener(v -> binding.signatureVendore.clearCanvas());
        binding.addEmail.setOnClickListener(v -> {
            EditText text = new EditText(getContext());
            text.setBackgroundResource(R.drawable.edit_text_back);
            text.setHint(getContext().getResources().getString(R.string.email_id));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(0, 10, 0, 10);
            text.setLayoutParams(lp);
            binding.emailContainer.addView(text);
            index++;
        });
        binding.cancelEmail.setOnClickListener(v -> {
            binding.lay1.setVisibility(View.VISIBLE);
            binding.lay2.setVisibility(View.GONE);
            binding.lay3.setVisibility(View.GONE);
        });
        binding.cancel.setOnClickListener(v -> {
            binding.lay1.setVisibility(View.VISIBLE);
            binding.lay2.setVisibility(View.GONE);
            binding.lay3.setVisibility(View.GONE);
        });
        binding.saveDraft.setOnClickListener(v -> {
            binding.lay3.setVisibility(View.VISIBLE);
            binding.lay1.setVisibility(View.GONE);
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}