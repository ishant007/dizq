package com.kamakhya.dibzi.ui.setting.fragment.scheduleshipment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.ShipmentIdsListItemBinding;
import com.kamakhya.dibzi.retrofitModel.getAllShipmentIds.ResultsItem;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.InspectionsFragment;

import java.util.ArrayList;
import java.util.List;

public class ScheduleShipmentAdapter extends RecyclerView.Adapter<ScheduleShipmentAdapter.ShipmentIdsViewHolder> {
    private Context context;
    private List<ResultsItem> shipmentIdsList;

    public ScheduleShipmentAdapter(Context context, List<ResultsItem> resultsItems) {
        this.context = context;
        this.shipmentIdsList = resultsItems;
    }

    public void UpdateList(List<ResultsItem> resultsItems) {
        this.shipmentIdsList = resultsItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShipmentIdsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShipmentIdsListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.shipment_ids_list_item, parent, false);
        return new ShipmentIdsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShipmentIdsViewHolder holder, int position) {
        holder.OnViewBind(shipmentIdsList.get(position));
    }

    @Override
    public int getItemCount() {
        return shipmentIdsList.size();
    }

    class ShipmentIdsViewHolder extends RecyclerView.ViewHolder {
        private ShipmentIdsListItemBinding binding;

        public ShipmentIdsViewHolder(@NonNull ShipmentIdsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void OnViewBind(final ResultsItem resultsItem) {
            binding.shipmentId.setText(resultsItem.getText());
            binding.shipmentId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((SettingActivity)context).startFragment(InspectionsFragment.newInstance(binding.shipmentId.getText().toString().trim(), "0"), true, InspectionsFragment.newInstance(binding.shipmentId.getText().toString().trim(), "0").toString());
                }
            });
        }
    }
}
