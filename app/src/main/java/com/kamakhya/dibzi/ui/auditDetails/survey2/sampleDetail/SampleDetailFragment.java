package com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentSampleDetailBinding;
import com.kamakhya.dibzi.databinding.SelectLayCustomBinding;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SampleDetailFragment extends BaseFragment<FragmentSampleDetailBinding, AuditDetailViewmodel> {
    private static SampleDetailFragment instance;
    private FragmentSampleDetailBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Dialog selectIonDialog;
    private SelectionDilaogAdapter dilaogAdapter;
    private SelectLayCustomBinding searchViewBinding;
    private String selction;
    private List<String> getWashingWetProcess = new ArrayList<>();
    private List<String> getWashingDryProcess = new ArrayList<>();
    private List<String> getmerechendise = new ArrayList<>();
    private List<String> getNoriankaTech = new ArrayList<>();

    ;

    public SampleDetailFragment() {
        // Required empty public constructor
    }

    public static SampleDetailFragment newInstance() {
        instance = instance == null ? new SampleDetailFragment() : instance;
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return SampleDetailFragment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sample_detail;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        searchViewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_lay_custom, null, false);
        binding.blackSealbtn.buttonHeader.setText(R.string.black_seal);
        binding.ppsamblebtn.buttonHeader.setText(R.string.pp_sample);
        binding.sizesetBtn.buttonHeader.setText(R.string.size_set);
        binding.prodSamplebtn.buttonHeader.setText(R.string.prod_sample);
        binding.goldSealBtn.buttonHeader.setText(R.string.gold_seal_sample);
        binding.prototypeBtn.buttonHeader.setText(R.string.proto_type);
        binding.fitSmpleBtn.buttonHeader.setText(R.string.fit_sample);
        clickListners();
        addDataIntoLists();
        DialogSelectReason();
    }

    private void clickListners() {
        binding.selectDate.setOnClickListener(v -> showDateTimePicker());
        binding.WashingWetSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVib().vibrate(100);
                dilaogAdapter = new SelectionDilaogAdapter(getContext(), getWashingWetProcess);
                searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
                selectIonDialog.show();
                searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getVib().vibrate(100);
                        selction = "";
                        if (dilaogAdapter.getSelectedString() != null) {
                            for (int i = 0; i < dilaogAdapter.getSelectedString().size(); i++) {
                                if(selction.isEmpty()){
                                    selction=dilaogAdapter.getSelectedString().get(i);
                                }else{
                                    selction = selction + "," + dilaogAdapter.getSelectedString().get(i);
                                }

                            }
                        }
                        selectIonDialog.dismiss();
                        binding.WashingWetTxt.setText(selction);

                    }
                });
            }
        });
        binding.washingDrySetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVib().vibrate(100);
                dilaogAdapter = new SelectionDilaogAdapter(getContext(), getWashingDryProcess);
                searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);

                selectIonDialog.show();
                searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getVib().vibrate(100);
                        selction = "";
                        if (dilaogAdapter.getSelectedString() != null) {
                            for (int i = 0; i < dilaogAdapter.getSelectedString().size(); i++) {
                                if(selction.isEmpty()){
                                    selction=dilaogAdapter.getSelectedString().get(i);
                                }else{
                                    selction = selction + "," + dilaogAdapter.getSelectedString().get(i);
                                }
                            }
                        }
                        selectIonDialog.dismiss();
                        binding.washingDryTxt.setText(selction);
                    }
                });
            }
        });
        binding.merchandiserSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVib().vibrate(100);
                dilaogAdapter = new SelectionDilaogAdapter(getContext(), getmerechendise);
                searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
                selectIonDialog.show();
                searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selction = "";
                        getVib().vibrate(100);
                        if (dilaogAdapter.getSelectedString() != null) {
                            for (int i = 0; i < dilaogAdapter.getSelectedString().size(); i++) {
                                if(selction.isEmpty()){
                                    selction=dilaogAdapter.getSelectedString().get(i);
                                }else{
                                    selction = selction + "," + dilaogAdapter.getSelectedString().get(i);
                                }
                            }
                        }
                        selectIonDialog.dismiss();
                        binding.merchandiserTxt.setText(selction);
                    }
                });
            }
        });
        binding.norlankaSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVib().vibrate(100);
                dilaogAdapter = new SelectionDilaogAdapter(getContext(), getNoriankaTech);
                searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
                selectIonDialog.show();
                searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getVib().vibrate(100);
                        selction = "";
                        if (dilaogAdapter.getSelectedString() != null) {
                            for (int i = 0; i < dilaogAdapter.getSelectedString().size(); i++) {
                                if(selction.isEmpty()){
                                    selction=dilaogAdapter.getSelectedString().get(i);
                                }else{
                                    selction = selction + "," + dilaogAdapter.getSelectedString().get(i);
                                }
                            }
                        }

                        selectIonDialog.dismiss();
                        binding.norlankaTxt.setText(selction);
                    }
                });
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                binding.deliveryDateTxt.setText(CommonUtils.getFormattedDate(getActivity(), date.getTimeInMillis(), "yyyy-MM-dd"));
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

    }

    private void DialogSelectReason() {

        selectIonDialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        selectIonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectIonDialog.setContentView(searchViewBinding.getRoot());
        dilaogAdapter = new SelectionDilaogAdapter(getContext(), new ArrayList<>());
        searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
        searchViewBinding.closeBtn.setOnClickListener(v -> {
            selectIonDialog.dismiss();
        });
        searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIonDialog.dismiss();
            }
        });
    }

    private void addDataIntoLists() {
        //wet
        getWashingWetProcess = new ArrayList<>();
        getWashingWetProcess.add("Washes");
        getWashingWetProcess.add("Normal Garment wash ");
        getWashingWetProcess.add("Enzyme wash");
        getWashingWetProcess.add("Stone Wash ");
        getWashingWetProcess.add("Bleach Wash");
        getWashingWetProcess.add("Tint wash");
        getWashingWetProcess.add("Performance wash");
        //dry
        getWashingDryProcess = new ArrayList<>();
        getWashingDryProcess.add("Reactive Dry");
        getWashingDryProcess.add("Pigment Dry");
        getWashingDryProcess.add("Direct Dry");
        getWashingDryProcess.add("Wave Dry");
        getWashingDryProcess.add("Fashionable Dry (Tie Dry/Dip Dry/Drip Dry )");
        getWashingDryProcess.add("Ozone");
        getWashingDryProcess.add("Laser");
        getWashingDryProcess.add("Tacking");
        getWashingDryProcess.add("Grinding");
        getWashingDryProcess.add("Hand Sand");
        getWashingDryProcess.add("PP Spray");
        getWashingDryProcess.add("Wishker");
        getWashingDryProcess.add("Dashtroyed/Distress");
        getWashingDryProcess.add("Color Spray");

        getmerechendise = new ArrayList<>();
        getmerechendise.add("Company1");
        getmerechendise.add("Company2");
        getmerechendise.add("Company3");
        getmerechendise.add("Company4");
        getNoriankaTech = new ArrayList<>();
        getNoriankaTech.add("Name1");
        getNoriankaTech.add("Name2");
        getNoriankaTech.add("Name3");
        getNoriankaTech.add("Name4");
        getNoriankaTech.add("Name5");

    }
}