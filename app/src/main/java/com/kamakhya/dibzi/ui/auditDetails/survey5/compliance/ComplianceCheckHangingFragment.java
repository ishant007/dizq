package com.kamakhya.dibzi.ui.auditDetails.survey5.compliance;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentComplianceCheckHangingBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class ComplianceCheckHangingFragment extends BaseFragment<FragmentComplianceCheckHangingBinding,AuditDetailViewmodel> {
    private static ComplianceCheckHangingFragment fragment;
    private FragmentComplianceCheckHangingBinding binding;
    private AuditDetailViewmodel viewmodel;



    public ComplianceCheckHangingFragment() {
        // Required empty public constructor
    }


    public static ComplianceCheckHangingFragment newInstance() {
        fragment = fragment == null ? new ComplianceCheckHangingFragment() : fragment;
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_compliance_check_hanging;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}