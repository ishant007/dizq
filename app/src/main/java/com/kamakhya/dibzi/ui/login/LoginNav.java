package com.kamakhya.dibzi.ui.login;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
public interface LoginNav {
    void showLoading(boolean b);

    void showMessage(String message);

    void startHome();
}
