package com.kamakhya.dibzi.ui.auditDetails.survey4.pilot1offdatafragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentPilot1StOffDataBinding;
import com.kamakhya.dibzi.databinding.SelectLayCustomBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;


public class Pilot1StOffDataFragment extends BaseFragment<FragmentPilot1StOffDataBinding, AuditDetailViewmodel> {
    private static Pilot1StOffDataFragment instance;
    private FragmentPilot1StOffDataBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Dialog selectIonDialog;
    private SelectionDilaogAdapter dilaogAdapter;
    private SelectLayCustomBinding searchViewBinding;

    public Pilot1StOffDataFragment() {
        // Required empty public constructor
    }

    public static Pilot1StOffDataFragment newInstance() {
        instance = instance == null ? new Pilot1StOffDataFragment() : instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_pilot1_st_off_data;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getViewDataBinding()!=null) {
            binding = getViewDataBinding();
            searchViewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_lay_custom, null, false);
            DialogSelect();
            binding.customerSetting.setOnClickListener(v -> selectIonDialog.show());
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private void DialogSelect() {

        selectIonDialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        selectIonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectIonDialog.setContentView(searchViewBinding.getRoot());
        dilaogAdapter = new SelectionDilaogAdapter(getContext(), new ArrayList<>());
        searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
        searchViewBinding.closeBtn.setOnClickListener(v -> {
            selectIonDialog.dismiss();
        });
        searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIonDialog.dismiss();
            }
        });
    }
}