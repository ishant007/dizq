package com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.GarmentsItemBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.modelclass.GarmentItem;

import java.util.ArrayList;
import java.util.List;

public class GarmentAdapter extends RecyclerView.Adapter<GarmentAdapter.GarmentViewHolder> {
    private List<GarmentItem> AllGarmentsItem = new ArrayList<>();
    private Context context;

    public GarmentAdapter(Context context) {
        this.context = context;
    }

    public void UpdateList(GarmentItem garmentItem) {
        AllGarmentsItem.add(garmentItem);
        notifyDataSetChanged();
    }

    public void RemoveGarmentsItem(int pos) {
        AllGarmentsItem.remove(pos);
        notifyDataSetChanged();
    }

    public List<GarmentItem> getAllGarments() {
        return AllGarmentsItem;
    }

    @NonNull
    @Override
    public GarmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GarmentsItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.garments_item,parent,false);
        return new GarmentViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GarmentViewHolder holder,final int position) {
        holder.OnbindItem(AllGarmentsItem.get(position),position);
    }

    @Override
    public int getItemCount() {
        return AllGarmentsItem.size();
    }

    class GarmentViewHolder extends RecyclerView.ViewHolder {
        private  GarmentsItemBinding binding;
        public GarmentViewHolder(@NonNull  GarmentsItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void OnbindItem(GarmentItem garmentItem,int position) {
            if(!garmentItem.getUrlOfPicture().isEmpty()){
                binding.addImgTxt.setVisibility(View.GONE);
                Glide.with(context).load(garmentItem.getUrlOfPicture()).placeholder(R.drawable.img_placeholder_big).into(binding.imageGarments);
            }
            garmentItem.setId(position);
            binding.close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RemoveGarmentsItem(position);
                }
            });
            binding.imageGarments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
        void CallCamera(){

        }
        void CallGallery(){

        }
    }
}
