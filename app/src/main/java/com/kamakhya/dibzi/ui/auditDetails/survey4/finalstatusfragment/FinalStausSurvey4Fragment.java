package com.kamakhya.dibzi.ui.auditDetails.survey4.finalstatusfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentFinalStausSurvey4Binding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class FinalStausSurvey4Fragment extends BaseFragment<FragmentFinalStausSurvey4Binding,AuditDetailViewmodel> {

private FragmentFinalStausSurvey4Binding binding;
private AuditDetailViewmodel viewmodel;
private static FinalStausSurvey4Fragment instance;
    public FinalStausSurvey4Fragment() {
        // Required empty public constructor
    }


    public static FinalStausSurvey4Fragment newInstance() {
        instance= instance==null? new FinalStausSurvey4Fragment():instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_final_staus_survey4;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=viewmodel==null?new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity()):viewmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.notApplicable.buttonHeader.setText(R.string.not_applicable);
        binding.unstaisfy.buttonHeader.setText(R.string.unstasfy);
        binding.satasfactory.buttonHeader.setText(R.string.satsfactory);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}