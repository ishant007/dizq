package com.kamakhya.dibzi.ui.schedules;

import com.kamakhya.dibzi.retrofitModel.getSchedule.ResultsItem;

import java.util.List;

/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
public interface ScheduleNav {
    void ClickOnEditButton(ResultsItem schedule, int position);

    void ClickOnScheduleDetail(ResultsItem schedule, int position);

    void showLoading(boolean b);

    void Logout();

    void showMessage(String server_not_responding);
    void Error(String message);

    void GetSchedules(List<ResultsItem> results);

    void ClickOnDeleteScheduleDetail(ResultsItem schedule, int position);

    void DeleteSchedule(String returnMessage, int pos);
}
