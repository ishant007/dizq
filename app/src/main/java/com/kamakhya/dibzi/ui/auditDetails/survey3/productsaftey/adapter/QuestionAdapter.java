package com.kamakhya.dibzi.ui.auditDetails.survey3.productsaftey.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.SurveyQuestionLayBinding;
public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {
    private Context context;

    public QuestionAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SurveyQuestionLayBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.survey_question_lay, parent, false);
        return new QuestionViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        holder.onBindView(position);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class QuestionViewHolder extends RecyclerView.ViewHolder {
        private SurveyQuestionLayBinding binding;

        public QuestionViewHolder(@NonNull SurveyQuestionLayBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onBindView(int position) {
            binding.yesBtn.buttonHeader.setText(R.string.yes);
            binding.NoBtn.buttonHeader.setText(R.string.no);
        }
    }
}
