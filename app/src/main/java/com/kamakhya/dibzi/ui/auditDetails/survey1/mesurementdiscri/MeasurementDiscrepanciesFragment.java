package com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementdiscri;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentMeasurementDiscrepanciesBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;
import com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementdiscri.adapter.MesurementDiscrepanciesAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MeasurementDiscrepanciesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeasurementDiscrepanciesFragment extends BaseFragment<FragmentMeasurementDiscrepanciesBinding,AuditDetailViewmodel> {
    private AuditDetailViewmodel viewmodel;
    private FragmentMeasurementDiscrepanciesBinding binding;
    private static MeasurementDiscrepanciesFragment instance;
    private MesurementDiscrepanciesAdapter adapter;

    public MeasurementDiscrepanciesFragment() {
        // Required empty public constructor
    }

    @Override
    public String toString() {
        return MeasurementDiscrepanciesFragment.class.getName();
    }

    public static MeasurementDiscrepanciesFragment newInstance() {
        instance= instance== null?new MeasurementDiscrepanciesFragment():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_measurement_discrepancies;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new MesurementDiscrepanciesAdapter(getContext());
        binding.mesurementAndDiscrimencyRecycler.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}