package com.kamakhya.dibzi.ui.login;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.InputType;

import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseActivity;
import com.kamakhya.dibzi.databinding.ActivityMainBinding;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;

public class Login extends BaseActivity<ActivityMainBinding, LoginViewModel> implements LoginNav {
    private LoginViewModel viewModel;
    private ActivityMainBinding binding;
    private Dialog internetDialog;
    private Disposable disposable;
    private Activity activity;
    private boolean isVisibility = false;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public LoginViewModel getViewModel() {
        return viewModel = new LoginViewModel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getViewDataBinding() != null) {
            binding = getViewDataBinding();
            viewModel.setNavigator(this);
            activity = this;
            disposable = RxView.clicks(binding.btnLogin).throttleFirst(1000, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Unit>() {
                @Override
                public void accept(Unit unit) throws Exception {
                    if (CommonUtils.isValidEmail(binding.userName.getText().toString().trim())) {
                        showCustomAlert("Please Enter Valid Email !!");
                    } else if (binding.password.getText().toString().isEmpty()) {
                        showCustomAlert("Please Enter Password");
                    } else {
                        viewModel.Login(binding.userName.getText().toString().trim(), binding.password.getText().toString());
                    }
               /* */
                }
            });

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (internetDialog == null) {
            internetDialog = CommonUtils.InternetConnectionAlert(this, false);
        }
        if (isConnected) {
            internetDialog.dismiss();
        } else {
            internetDialog.show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void showMessage(String message) {
        showCustomAlert(message);

    }

    @Override
    public void startHome() {
        startActivity(new Intent(activity, SettingActivity.class));
        finish();
    }
}