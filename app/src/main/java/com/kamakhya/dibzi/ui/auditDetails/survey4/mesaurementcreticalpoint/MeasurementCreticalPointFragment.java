package com.kamakhya.dibzi.ui.auditDetails.survey4.mesaurementcreticalpoint;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentMeasurementCreticalPointBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

public class MeasurementCreticalPointFragment extends BaseFragment<FragmentMeasurementCreticalPointBinding,AuditDetailViewmodel> {
    private static MeasurementCreticalPointFragment fragment;
    private AuditDetailViewmodel viewmodel;
    private FragmentMeasurementCreticalPointBinding binding;


    public MeasurementCreticalPointFragment() {
        // Required empty public constructor
    }


    public static MeasurementCreticalPointFragment newInstance() {
        fragment= fragment== null?new MeasurementCreticalPointFragment():fragment;
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_measurement_cretical_point;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=viewmodel==null?new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity()):viewmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.minimumNectLay.hederTxt.setText(R.string.minimum_nect);
        binding.nectWidthLay.hederTxt.setText(R.string.nect_width);
        binding.chestLay.hederTxt.setText(R.string.chest);
        binding.bottomHemLay.hederTxt.setText(R.string.bottom_hem);
        binding.sleeveLay.hederTxt.setText(R.string.sleeve_length);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}