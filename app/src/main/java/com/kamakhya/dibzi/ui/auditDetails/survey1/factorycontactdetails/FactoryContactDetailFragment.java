package com.kamakhya.dibzi.ui.auditDetails.survey1.factorycontactdetails;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentFactoryContactDetailBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class FactoryContactDetailFragment extends BaseFragment<FragmentFactoryContactDetailBinding, AuditDetailViewmodel> {

    private FragmentFactoryContactDetailBinding binding;
    private AuditDetailViewmodel viewmodel;
    private static FactoryContactDetailFragment instance;

    public FactoryContactDetailFragment() {
        // Required empty public constructor
    }

    public static FactoryContactDetailFragment newInstance() {
        instance = instance == null ? new FactoryContactDetailFragment() : instance;
        return instance;
    }

    @Override
    public String toString() {
        return FactoryContactDetailFragment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_factory_contact_detail;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}