package com.kamakhya.dibzi.ui.auditDetails.survey5.measurement;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.AddSampleDialogBinding;
import com.kamakhya.dibzi.databinding.FragmentOtherObservationBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;


public class MeasurementFragment extends BaseFragment<FragmentOtherObservationBinding, AuditDetailViewmodel> {
private static MeasurementFragment instance;
private FragmentOtherObservationBinding binding;
private AuditDetailViewmodel viewmodel;
private Dialog sampleDialog;
private AddSampleDialogBinding  addSampleDialogBinding;


    public MeasurementFragment() {
        // Required empty public constructor
    }

    public static MeasurementFragment newInstance() {
        instance =instance ==null? new MeasurementFragment():instance;

        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_other_observation;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        addSampleDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.add_sample_dialog, null, false);
        DialogAddSample();
        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sampleDialog.show();
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private void DialogAddSample(){

        sampleDialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        sampleDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sampleDialog.setContentView(addSampleDialogBinding.getRoot());
        addSampleDialogBinding.closeBtn.setOnClickListener(v -> {
            sampleDialog.dismiss();
        });
        addSampleDialogBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sampleDialog.dismiss();
            }
        });
    }
}