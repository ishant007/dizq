package com.kamakhya.dibzi.ui.updateschedule;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;

import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseActivity;
import com.kamakhya.dibzi.databinding.ActivityUpdateScheduleBinding;
import com.kamakhya.dibzi.retrofitModel.getPoDetails.Result;
import com.kamakhya.dibzi.retrofitModel.getPoNumber.ResultsItem;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingNav;
import com.kamakhya.dibzi.ui.setting.activity.SettingViewmodel;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.AuditListAdapter;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.AuditorListAdapter;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.GarmentPoAdapter;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;

public class UpdateScheduleActivity extends BaseActivity<ActivityUpdateScheduleBinding, SettingViewmodel> implements SettingNav {
    private SettingViewmodel viewmodel;
    private ActivityUpdateScheduleBinding binding;
    private String currentTimeandDate = "";
    private Disposable disposable;
    private Dialog internetDialog;
    private GarmentPoAdapter adapter;
    private static String shipmentId = "";
    private static String slno = "0";
    private String companyId;
    private String pOSLNo;
    private boolean isDateSelected = false;
    private AuditorListAdapter auditorListAdapter;
    private AuditListAdapter auditListAdapter;
    private String type;

    @Override
    public int getBindingVariable() {
        return 1;
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_update_schedule;
    }

    @Override
    public SettingViewmodel getViewModel() {
        return viewmodel = new SettingViewmodel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        viewmodel.getAuditListData();
        viewmodel.getAuditorData();
        type = getIntent().getStringExtra("type");
        slno = getIntent().getStringExtra("slno");
        if (type != null && !type.isEmpty()) {
            if (type.equalsIgnoreCase("0")) {
                viewmodel.UpdateDataList(slno);
            } else {
                viewmodel.UpdateDataListByShipment(slno);
            }


        } else {
            onBackPressed();
            showMessage("Data Not Found");
        }
        binding.mainLay.setVisibility(View.GONE);

        startClickLisnersNow();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void startClickLisnersNow() {
        binding.toolbar.title.setText("Schedule Request");
        binding.toolbar.closeBtn.setVisibility(View.VISIBLE);
        binding.toolbar.saveBtn.setVisibility(View.VISIBLE);
        disposable = RxView.clicks(binding.toolbar.closeBtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                onBackPressed();
            }
        });
        disposable = RxView.clicks(binding.toolbar.saveBtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                if (isDateSelected) {
                    if (shipmentId != null && !shipmentId.isEmpty() && shipmentId.length() > 1) {
                        viewmodel.CreateScheduleByShipment(Long.parseLong(slno), shipmentId, binding.chooseDatemain.getText().toString().trim(), Integer.parseInt(companyId), binding.inspectionList.getText().toString().trim(), binding.garmentPoEdit.getText().toString().trim(), binding.auditiorEditText.getText().toString().trim(),
                                binding.FactoryEditText.getText().toString().trim(), binding.buyerPOEdit.getText().toString().trim(), binding.garmentVendorEditText.getText().toString().trim(), binding.productCatEditText.getText().toString().trim()
                                , binding.styleCodeEdit.getText().toString(), Double.parseDouble(binding.orderQtyEdit.getText().toString().trim()));
                    } else {
                        /* startActivity(new Intent(getBaseActivity(), SchedulesActivity.class).putExtra("type", sendTpeOfSurvey));*/
                        viewmodel.CreateRequest(Long.parseLong(slno), binding.chooseDatemain.getText().toString().trim(), Integer.parseInt(companyId), binding.inspectionList.getText().toString().trim(), Long.parseLong(pOSLNo), binding.auditiorEditText.getText().toString().trim(),
                                binding.customer.getText().toString().trim(), binding.FactoryEditText.getText().toString().trim(), binding.garmentVendorEditText.getText().toString().trim(), binding.productCatEditText.getText().toString().trim(),
                                binding.styleCodeEdit.getText().toString(), binding.disCriptionEdit.getText().toString().trim(), Double.parseDouble(binding.orderQtyEdit.getText().toString().trim()), binding.garmentPoEdit.getText().toString().trim());
                    }
                } else {
                    showCustomAlert("Please Select Schedule Date");
                }

            }
        });

        disposable = RxView.clicks(binding.chooseDate).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                showDateTimePicker();
            }
        });
        disposable = RxView.clicks(binding.chooseDatemain).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                showDateTimePicker();
            }
        });
        disposable = RxView.clicks(binding.inspectionList).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.spinnerInspection.performClick();
            }
        });
        disposable = RxView.clicks(binding.auditiorArrowDown).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.AuditiorSpinner.performClick();
            }
        });
        disposable = RxView.clicks(binding.arrowDown).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.spinnerInspection.performClick();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.toolbar.closeBtn.setVisibility(View.INVISIBLE);
        binding.toolbar.saveBtn.setVisibility(View.INVISIBLE);
    }

    /*public String showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        binding.chooseDatemain.setText(CommonUtils.getFormattedDate(getActivity(), date.getTimeInMillis()));
                        isDateSelected = true;
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
        return currentTimeandDate;
    }*/
    public String showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                binding.chooseDatemain.setText(CommonUtils.getFormattedDate(getContext(), date.getTimeInMillis(), "dd-MM-yyyy"));
                isDateSelected = true;
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
        return currentTimeandDate;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        try {
            if (isConnected) {
                getInternetDialog().dismiss();
            } else {
                getInternetDialog().show();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void UpdateGarmentPO(List<ResultsItem> results) {
        if (results != null && results.size() > 0) {
            adapter = new GarmentPoAdapter(this, R.layout.custom_spinner, results);
            binding.garmentPoSpinner.setAdapter(adapter);
            binding.garmentPoArrowDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.garmentPoSpinner.performClick();
                }
            });
            binding.garmentPoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    pOSLNo = results.get(position).getPOSLNumber();
                    binding.garmentPoEdit.setText(results.get(position).getPONumber());
                    viewmodel.callPoGarmentdetail(results.get(position).getPOSLNumber());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    @Override
    public void Logout() {
        getSharedPre().Logout();
        finish();
    }

    @Override
    public void showMessage(String message) {
        showCustomAlert(message);
    }

    @Override
    public void UpdateGarmentsDetails(Result result) {
        if (result != null) {
            companyId = String.valueOf(result.getCompanyId());
            if (result.getDescription() != null && !result.getDescription().isEmpty() && result.getDescription().length() > 0) {
                binding.disCriptionEdit.setText(result.getDescription().trim());
            }
            if (result.getFactory() != null && !result.getFactory().isEmpty() && result.getFactory().length() > 0) {
                binding.FactoryEditText.setText(result.getFactory().trim());
            } else {
                showMessage("Data not Found");
                onBackPressed();
            }
            if (result.getGarmentVendor() != null && !result.getGarmentVendor().isEmpty() && result.getGarmentVendor().length() > 0) {
                binding.garmentVendorEditText.setText(result.getGarmentVendor().trim());
            }
            if (result.getCustomer() != null && !result.getCustomer().isEmpty() && result.getCustomer().length() > 0) {
                binding.customer.setText(result.getCustomer().trim());
            }
            if (result.getProductCategory() != null && !result.getProductCategory().isEmpty() && result.getProductCategory().length() > 0) {
                binding.productCatEditText.setText(result.getProductCategory().trim());
            }
            if (result.getStyleCode() != null && !result.getStyleCode().isEmpty() && result.getStyleCode().length() > 0) {
                binding.styleCodeEdit.setText(result.getStyleCode().trim());
            }
            if (result.getOrderedQuantity() > 0) {
                binding.orderQtyEdit.setText(String.valueOf(result.getOrderedQuantity()));
            } else {
                binding.orderQtyEdit.setText(String.valueOf(0));
            }

        }
    }

    @Override
    public void AuditorData(List<com.kamakhya.dibzi.retrofitModel.getAuditorsData.ResultsItem> results) {
        auditorListAdapter = new AuditorListAdapter(this, R.layout.custom_spinner, results);
        binding.AuditiorSpinner.setAdapter(auditorListAdapter);

        binding.AuditiorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.auditiorEditText.setText(results.get(position).getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void AuditListData(List<com.kamakhya.dibzi.retrofitModel.getauditlist.ResultsItem> results) {
        auditListAdapter = new AuditListAdapter(this, R.layout.custom_spinner, results);
        binding.spinnerInspection.setAdapter(auditListAdapter);
        binding.spinnerInspection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.inspectionList.setText(results.get(position).getText());
                if (position == 2) {
                    binding.mainLay.setVisibility(View.VISIBLE);
                    binding.garmentPoLay.setVisibility(View.GONE);
                    binding.customerLay.setVisibility(View.GONE);
                    binding.productCatLay.setVisibility(View.GONE);
                    binding.styleCodeLay.setVisibility(View.GONE);
                    binding.orderQtyLay.setVisibility(View.GONE);
                    binding.discriptionLay.setVisibility(View.GONE);

                } else {
                    if (shipmentId != null && !shipmentId.isEmpty() && shipmentId.length() > 1) {
                        binding.buyerPoLay.setVisibility(View.VISIBLE);
                        binding.discriptionLay.setVisibility(View.GONE);
                        binding.customerLay.setVisibility(View.GONE);
                        binding.garmentPoArrowDown.setVisibility(View.GONE);
                    } else {
                        viewmodel.CallAllGarmentsPo();
                        binding.buyerPoLay.setVisibility(View.GONE);
                        binding.discriptionLay.setVisibility(View.VISIBLE);
                        binding.customerLay.setVisibility(View.VISIBLE);
                        binding.garmentPoArrowDown.setVisibility(View.VISIBLE);
                    }
                    binding.garmentPoLay.setVisibility(View.VISIBLE);
                    binding.mainLay.setVisibility(View.VISIBLE);
                    binding.productCatLay.setVisibility(View.VISIBLE);
                    binding.styleCodeLay.setVisibility(View.VISIBLE);
                    binding.orderQtyLay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void GetDataByShipmentId(com.kamakhya.dibzi.retrofitModel.getDataByShipmentId.Result result) {
        if (result != null) {
            companyId = String.valueOf(result.getCompanyId());
            shipmentId = String.valueOf(result.getShipmentId());
            if (result.getFactory() != null && !result.getFactory().isEmpty() && result.getFactory().length() > 0) {
                binding.FactoryEditText.setText(result.getFactory().trim());
            } else {
                showMessage("Data not Found");
                onBackPressed();
            }
            if (result.getSupplier() != null && !result.getSupplier().isEmpty() && result.getSupplier().length() > 0) {
                binding.garmentVendorEditText.setText(result.getSupplier().trim());
            }

            if (result.getProductCategory() != null && !result.getProductCategory().isEmpty() && result.getProductCategory().length() > 0) {
                binding.productCatEditText.setText(result.getProductCategory().trim());
            }
            if (result.getStyle() != null && !result.getStyle().isEmpty() && result.getStyle().length() > 0) {
                binding.styleCodeEdit.setText(result.getStyle().trim());
            }
            if (result.getBuyerPoNo() != null && !result.getBuyerPoNo().isEmpty() && result.getBuyerPoNo().length() > 0) {
                binding.buyerPOEdit.setText(result.getBuyerPoNo().trim());
            }
            if (result.getPoNumber() != null && !result.getPoNumber().isEmpty() && result.getPoNumber().length() > 0) {
                binding.garmentPoEdit.setText(result.getPoNumber().trim());
            }else{
                binding.garmentPoEdit.setText("");
            }
            if (result.getOfferedQuantity() > 0) {
                binding.orderQtyEdit.setText(String.valueOf(result.getOfferedQuantity()));
            } else {
                binding.orderQtyEdit.setText(String.valueOf(0));
            }

        }
    }

    @Override
    public void ScheduleCreated() {
        onBackPressed();
    }

    @Override
    public void UpdateSchedule(com.kamakhya.dibzi.retrofitModel.updateSchedule.Result result) {
        if (result != null) {
            companyId = String.valueOf(result.getCompanyId());
            pOSLNo= String.valueOf(result.getPoSlNo());
            binding.garmentPoEdit.setText(result.getPoNumber());
            if (result.getDescription() != null && !result.getDescription().isEmpty() && result.getDescription().length() > 0) {
                binding.disCriptionEdit.setText(result.getDescription().trim());
            }
            if (result.getFactory() != null && !result.getFactory().isEmpty() && result.getFactory().length() > 0) {
                binding.FactoryEditText.setText(result.getFactory().trim());
            } else {
                showMessage("Data not Found");
            }
            if (result.getGarmentVendor() != null && !result.getGarmentVendor().isEmpty() && result.getGarmentVendor().length() > 0) {
                binding.garmentVendorEditText.setText(result.getGarmentVendor().trim());
            }
            if (result.getCustomer() != null && !result.getCustomer().isEmpty() && result.getCustomer().length() > 0) {
                binding.customer.setText(result.getCustomer().trim());
            }
            if (result.getProductCategory() != null && !result.getProductCategory().isEmpty() && result.getProductCategory().length() > 0) {
                binding.productCatEditText.setText(result.getProductCategory().trim());
            }
            if (result.getStyleCode() != null && !result.getStyleCode().isEmpty() && result.getStyleCode().length() > 0) {
                binding.styleCodeEdit.setText(result.getStyleCode().trim());
            }
            if (result.getOrderedQuantity() > 0) {
                binding.orderQtyEdit.setText(String.valueOf(result.getOrderedQuantity()));
            } else {
                binding.orderQtyEdit.setText(String.valueOf(0));
            }

        }
    }
}