package com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentPart1DetailBinding;
import com.kamakhya.dibzi.databinding.SelectLayCustomBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail.adapter.Part1Adapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;


public class Part1DetailFragment extends BaseFragment<FragmentPart1DetailBinding, AuditDetailViewmodel> {
    private FragmentPart1DetailBinding binding;
    private static Part1DetailFragment instance;
    private AuditDetailViewmodel viewmodel;
    private Part1Adapter adapter;
    private Dialog selectIonDialog;
    private SelectionDilaogAdapter dilaogAdapter;
    private SelectLayCustomBinding searchViewBinding;
    private String selction;

    public Part1DetailFragment() {
        // Required empty public constructor
    }

    public static Part1DetailFragment newInstance() {
        instance = instance == null ? new Part1DetailFragment() : instance;
        return instance;
    }

    @Override
    public String toString() {
        return getString(R.string.part_1);
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_part1_detail;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        adapter = new Part1Adapter(getContext(),0);
        searchViewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_lay_custom, null, false);
        DialogSelectReason();
        binding.part1DetailRecycler.setAdapter(adapter);
        binding.customerProgresslay.hederTxt.setText(getString(R.string.customer_in_progress));
        binding.customerProgresslay.addCommentTxt.setText("");
        binding.customerProgresslay.addCommentTxt.setHint("Select");
        binding.customerProgresslay.Addcomment.setImageResource(R.drawable.sliders);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    private void DialogSelectReason() {

        selectIonDialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        selectIonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectIonDialog.setContentView(searchViewBinding.getRoot());
        dilaogAdapter = new SelectionDilaogAdapter(getContext(), new ArrayList<>());
        searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
        searchViewBinding.closeBtn.setOnClickListener(v -> {
            selectIonDialog.dismiss();
        });
        searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIonDialog.dismiss();
            }
        });
    }
}