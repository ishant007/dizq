package com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentPart2DetailBinding;
import com.kamakhya.dibzi.databinding.FragmentPart2PopperBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail.adapter.Part1Adapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class Part2DetailFragment extends BaseFragment<FragmentPart2DetailBinding,AuditDetailViewmodel> {
    private static Part2DetailFragment instance;
    private Part1Adapter adapter;
    private FragmentPart2DetailBinding binding;
    private AuditDetailViewmodel viewmodel;



    public Part2DetailFragment() {
        // Required empty public constructor
    }


    public static Part2DetailFragment newInstance() {
        instance= instance ==null? new Part2DetailFragment():instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_part2_detail;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new Part1Adapter(getContext(),3);
        binding.par2PooperRecycler.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}