package com.kamakhya.dibzi.ui.auditDetails.survey2.making;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.CommentDilaogLayBinding;
import com.kamakhya.dibzi.databinding.FragmentMakingBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MakingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MakingFragment extends BaseFragment<FragmentMakingBinding,AuditDetailViewmodel> {
private static MakingFragment instance;
private FragmentMakingBinding binding;
private AuditDetailViewmodel viewmodel;
    private Dialog commentDialog;
    private CommentDilaogLayBinding bindingDialog;
    public MakingFragment() {
        // Required empty public constructor
    }


    public static MakingFragment newInstance() {
        instance= instance==null ?new MakingFragment():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_making;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.comment_dilaog_lay, null, false);
        GetCommentDialog();
        DesigningSetup();
        clickListners();
    }

    private void DesigningSetup() {
        binding.masurementLay.HeaderName.setText(getResources().getString(R.string.button));
        binding.spiLay.HeaderName.setText(getResources().getString(R.string.pinch_setting));
        binding.topStichingLay.HeaderName.setText(getResources().getString(R.string.sanps_poppers));
        binding.trimmings.HeaderName.setText(getResources().getString(R.string.thread));
        binding.workmanShip.HeaderName.setText(getResources().getString(R.string.workmanships));
        //correctBtn
        binding.masurementLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.spiLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.topStichingLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.trimmings.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.workmanShip.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        //incorreect
        binding.masurementLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.spiLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.topStichingLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.trimmings.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.workmanShip.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        //not acceptable
        binding.masurementLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.spiLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.topStichingLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.trimmings.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.workmanShip.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
    }

    private void clickListners() {
        binding.masurementLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();

            }
        });
        binding.spiLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.topStichingLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.trimmings.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.workmanShip.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private Dialog GetCommentDialog() {
        if (commentDialog == null) {
            commentDialog = new Dialog(getContext());
            commentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commentDialog.setContentView(bindingDialog.getRoot());
            final Window window = commentDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            bindingDialog.close.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
            bindingDialog.cancel.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
        }
        return commentDialog;
    }
}