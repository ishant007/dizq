package com.kamakhya.dibzi.ui.setting.fragment.inspections;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentInspectionsBinding;
import com.kamakhya.dibzi.retrofitModel.getPoDetails.Result;
import com.kamakhya.dibzi.retrofitModel.getPoNumber.ResultsItem;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.schedules.SchedulesActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingNav;
import com.kamakhya.dibzi.ui.setting.activity.SettingViewmodel;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.AuditListAdapter;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.AuditorListAdapter;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.adapter.GarmentPoAdapter;
import com.kamakhya.dibzi.ui.setting.fragment.scheduleshipment.ScheduleShipment;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;


public class InspectionsFragment extends BaseFragment<FragmentInspectionsBinding, SettingViewmodel> implements SettingNav {
    private static InspectionsFragment instance;
    private FragmentInspectionsBinding binding;
    private SettingViewmodel viewmodel;
    private String currentTimeandDate = "";
    private Disposable disposable;
    private Dialog internetDialog;
    private GarmentPoAdapter adapter;
    private static String shipmentId = "";
    private static String slno = "0";
    private String companyId;
    private String pOSLNo;
    private boolean isDateSelected = false;
    private AuditorListAdapter auditorListAdapter;
    private AuditListAdapter auditListAdapter;

    public InspectionsFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public String toString() {
        return InspectionsFragment.class.getName();
    }

    public static InspectionsFragment newInstance(String shimpmentIdMain, String slnoMain) {
        slno = slnoMain;
        shipmentId = shimpmentIdMain;
        instance = instance == null ? new InspectionsFragment() : instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_inspections;
    }

    @Override
    public SettingViewmodel getViewModel() {
        return viewmodel = new SettingViewmodel(getContext(), getSharedPre(), getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        binding.mainLay.setVisibility(View.GONE);
        viewmodel.setNavigator(this);
        viewmodel.getAuditListData();
        viewmodel.getAuditorData();
        startClickLisnersNow();
    }

    private void startClickLisnersNow() {
        ((SettingActivity) getBaseActivity()).getToolbar().title.setText("Schedule Request");
        ((SettingActivity) getBaseActivity()).getToolbar().closeBtn.setVisibility(View.VISIBLE);
        ((SettingActivity) getBaseActivity()).getToolbar().saveBtn.setVisibility(View.VISIBLE);
        disposable = RxView.clicks(((SettingActivity) getBaseActivity()).getToolbar().closeBtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                getBaseActivity().onBackPressed();
            }
        });
        disposable = RxView.clicks(((SettingActivity) getBaseActivity()).getToolbar().saveBtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                if (isDateSelected) {
                    if (shipmentId != null && !shipmentId.isEmpty() && shipmentId.length() > 1) {
                            viewmodel.CreateScheduleByShipment(Long.parseLong(slno), shipmentId, binding.chooseDatemain.getText().toString().trim(), Integer.parseInt(companyId), binding.inspectionList.getText().toString().trim(), binding.garmentPoEdit.getText().toString().trim(), binding.auditiorEditText.getText().toString().trim(),
                                    binding.FactoryEditText.getText().toString().trim(), binding.buyerPOEdit.getText().toString().trim(), binding.garmentVendorEditText.getText().toString().trim(), binding.productCatEditText.getText().toString().trim()
                                    , binding.styleCodeEdit.getText().toString(), Double.parseDouble(binding.orderQtyEdit.getText().toString().trim()));
                    } else {
                            /* startActivity(new Intent(getBaseActivity(), SchedulesActivity.class).putExtra("type", sendTpeOfSurvey));*/
                            viewmodel.CreateRequest(Long.parseLong(slno), binding.chooseDatemain.getText().toString().trim(), Integer.parseInt(companyId), binding.inspectionList.getText().toString().trim(), Long.parseLong(pOSLNo), binding.auditiorEditText.getText().toString().trim(),
                                    binding.customer.getText().toString().trim(), binding.FactoryEditText.getText().toString().trim(), binding.garmentVendorEditText.getText().toString().trim(), binding.productCatEditText.getText().toString().trim(),
                                    binding.styleCodeEdit.getText().toString(), binding.disCriptionEdit.getText().toString().trim(), Double.parseDouble(binding.orderQtyEdit.getText().toString().trim()), binding.garmentPoEdit.getText().toString().trim());
                    }
                } else {
                    showCustomAlert("Please Select Schedule Date");
                }

            }
        });

        disposable = RxView.clicks(binding.chooseDate).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                showDateTimePicker();
            }
        });
        disposable = RxView.clicks(binding.chooseDatemain).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                showDateTimePicker();
            }
        });
        disposable = RxView.clicks(binding.inspectionList).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.spinnerInspection.performClick();
            }
        });
        disposable = RxView.clicks(binding.auditiorArrowDown).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.AuditiorSpinner.performClick();
            }
        });
        disposable = RxView.clicks(binding.arrowDown).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.spinnerInspection.performClick();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((SettingActivity) getBaseActivity()).getToolbar().closeBtn.setVisibility(View.INVISIBLE);
        ((SettingActivity) getBaseActivity()).getToolbar().saveBtn.setVisibility(View.INVISIBLE);
    }

    /*public String showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        binding.chooseDatemain.setText(CommonUtils.getFormattedDate(getActivity(), date.getTimeInMillis()));
                        isDateSelected = true;
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
        return currentTimeandDate;
    }*/
    public String showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                binding.chooseDatemain.setText(CommonUtils.getFormattedDate(getActivity(), date.getTimeInMillis(),"dd-MM-yyyy"));
                isDateSelected = true;
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
        return currentTimeandDate;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void UpdateGarmentPO(List<ResultsItem> results) {
        if (results != null && results.size() > 0) {
            adapter = new GarmentPoAdapter(getContext(), R.layout.custom_spinner, results);
            binding.garmentPoSpinner.setAdapter(adapter);
            binding.garmentPoArrowDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.garmentPoSpinner.performClick();
                }
            });
            binding.garmentPoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    pOSLNo = results.get(position).getPOSLNumber();
                    binding.garmentPoEdit.setText(results.get(position).getPONumber());
                    viewmodel.callPoGarmentdetail(results.get(position).getPOSLNumber());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    @Override
    public void Logout() {
        getSharedPre().Logout();
        getBaseActivity().finish();
    }

    @Override
    public void showMessage(String message) {
        showCustomAlert(message);
    }

    @Override
    public void UpdateGarmentsDetails(Result result) {
        if (result != null) {
            companyId = String.valueOf(result.getCompanyId());
            if (result.getDescription() != null && !result.getDescription().isEmpty() && result.getDescription().length() > 0) {
                binding.disCriptionEdit.setText(result.getDescription().trim());
            }
            if (result.getFactory() != null && !result.getFactory().isEmpty() && result.getFactory().length() > 0) {
                binding.FactoryEditText.setText(result.getFactory().trim());
            } else {
                showMessage("Data not Found");
            }
            if (result.getGarmentVendor() != null && !result.getGarmentVendor().isEmpty() && result.getGarmentVendor().length() > 0) {
                binding.garmentVendorEditText.setText(result.getGarmentVendor().trim());
            }
            if (result.getCustomer() != null && !result.getCustomer().isEmpty() && result.getCustomer().length() > 0) {
                binding.customer.setText(result.getCustomer().trim());
            }
            if (result.getProductCategory() != null && !result.getProductCategory().isEmpty() && result.getProductCategory().length() > 0) {
                binding.productCatEditText.setText(result.getProductCategory().trim());
            }
            if (result.getStyleCode() != null && !result.getStyleCode().isEmpty() && result.getStyleCode().length() > 0) {
                binding.styleCodeEdit.setText(result.getStyleCode().trim());
            }
            if (result.getOrderedQuantity() > 0) {
                binding.orderQtyEdit.setText(String.valueOf(result.getOrderedQuantity()));
            } else {
                binding.orderQtyEdit.setText(String.valueOf(0));
            }

        }
    }

    @Override
    public void AuditorData(List<com.kamakhya.dibzi.retrofitModel.getAuditorsData.ResultsItem> results) {
        auditorListAdapter = new AuditorListAdapter(getContext(), R.layout.custom_spinner, results);
        binding.AuditiorSpinner.setAdapter(auditorListAdapter);

        binding.AuditiorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.auditiorEditText.setText(results.get(position).getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void AuditListData(List<com.kamakhya.dibzi.retrofitModel.getauditlist.ResultsItem> results) {
        auditListAdapter = new AuditListAdapter(getContext(), R.layout.custom_spinner, results);
        binding.spinnerInspection.setAdapter(auditListAdapter);
        binding.spinnerInspection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.inspectionList.setText(results.get(position).getText());
                if (position == 2) {
                    binding.mainLay.setVisibility(View.VISIBLE);
                    binding.garmentPoLay.setVisibility(View.GONE);
                    binding.customerLay.setVisibility(View.GONE);
                    binding.productCatLay.setVisibility(View.GONE);
                    binding.styleCodeLay.setVisibility(View.GONE);
                    binding.orderQtyLay.setVisibility(View.GONE);
                    binding.discriptionLay.setVisibility(View.GONE);

                } else {
                    if (shipmentId != null && !shipmentId.isEmpty() && shipmentId.length() > 1) {
                        viewmodel.CallDataShipmentId(shipmentId);
                        binding.buyerPoLay.setVisibility(View.VISIBLE);
                        binding.discriptionLay.setVisibility(View.GONE);
                        binding.customerLay.setVisibility(View.GONE);
                        binding.garmentPoArrowDown.setVisibility(View.GONE);
                    } else {
                        viewmodel.CallAllGarmentsPo();
                        binding.buyerPoLay.setVisibility(View.GONE);
                        binding.discriptionLay.setVisibility(View.VISIBLE);
                        binding.customerLay.setVisibility(View.VISIBLE);
                        binding.garmentPoArrowDown.setVisibility(View.VISIBLE);
                    }
                    binding.garmentPoLay.setVisibility(View.VISIBLE);
                    binding.mainLay.setVisibility(View.VISIBLE);
                    binding.productCatLay.setVisibility(View.VISIBLE);
                    binding.styleCodeLay.setVisibility(View.VISIBLE);
                    binding.orderQtyLay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void GetDataByShipmentId(com.kamakhya.dibzi.retrofitModel.getDataByShipmentId.Result result) {
        if (result != null) {
            companyId = String.valueOf(result.getCompanyId());

            if (result.getFactory() != null && !result.getFactory().isEmpty() && result.getFactory().length() > 0) {
                binding.FactoryEditText.setText(result.getFactory().trim());
            } else {
                showMessage("Data not Found");
            }
            if (result.getSupplier() != null && !result.getSupplier().isEmpty() && result.getSupplier().length() > 0) {
                binding.garmentVendorEditText.setText(result.getSupplier().trim());
            }

            if (result.getProductCategory() != null && !result.getProductCategory().isEmpty() && result.getProductCategory().length() > 0) {
                binding.productCatEditText.setText(result.getProductCategory().trim());
            }
            if (result.getStyle() != null && !result.getStyle().isEmpty() && result.getStyle().length() > 0) {
                binding.styleCodeEdit.setText(result.getStyle().trim());
            }
            if (result.getBuyerPoNo() != null && !result.getBuyerPoNo().isEmpty() && result.getBuyerPoNo().length() > 0) {
                binding.buyerPOEdit.setText(result.getBuyerPoNo().trim());
            }
            if (result.getPoNumber() != null && !result.getPoNumber().isEmpty() && result.getPoNumber().length() > 0) {
                binding.garmentPoEdit.setText(result.getPoNumber().trim());
            }
            if (result.getOfferedQuantity() > 0) {
                binding.orderQtyEdit.setText(String.valueOf(result.getOfferedQuantity()));
            } else {
                binding.orderQtyEdit.setText(String.valueOf(0));
            }

        }
    }

    @Override
    public void ScheduleCreated() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void UpdateSchedule(com.kamakhya.dibzi.retrofitModel.updateSchedule.Result result) {

    }
}