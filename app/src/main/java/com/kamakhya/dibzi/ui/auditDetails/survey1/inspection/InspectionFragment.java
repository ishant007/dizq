package com.kamakhya.dibzi.ui.auditDetails.survey1.inspection;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.CommentDilaogLayBinding;
import com.kamakhya.dibzi.databinding.FragmentInspectionBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;

public class InspectionFragment extends BaseFragment<FragmentInspectionBinding, AuditDetailViewmodel> {

    private static InspectionFragment instance;
    private FragmentInspectionBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Disposable disposable;
    private CommentDilaogLayBinding bindingDialog;
    private Dialog commentDialog;

    @Override
    public String toString() {
        return InspectionFragment.class.getName();
    }

    public InspectionFragment() {

    }

    public static InspectionFragment newInstance() {
        instance = instance == null ? new InspectionFragment() : instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_inspection;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.comment_dilaog_lay, null, false);
        commentDialog=GetCommentDialog();
        binding.Button1St.inspectionTotalTxt.setText(getResources().getString(R.string.st1));
        binding.Button2nd.inspectionTotalTxt.setText(getResources().getString(R.string.nd2));
        binding.Button3rd.inspectionTotalTxt.setText(getResources().getString(R.string.rd3));
        binding.Button4th.inspectionTotalTxt.setText(getResources().getString(R.string.th4));
        binding.cutQualityLay.hederTxt.setText(getResources().getString(R.string.cut_quality));
        binding.stichedQtyLay.hederTxt.setText(getResources().getString(R.string.stiched_qty));
        binding.packedQtyLay.hederTxt.setText(getResources().getString(R.string.packed_qty));
        binding.colorRefLay.hederTxt.setText(getResources().getString(R.string.color_ref));
        binding.Button1St.buttonLay.setBackgroundColor(getResources().getColor(R.color.green_app));
        disposable = RxView.clicks(binding.Button1St.buttonLay).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.Button1St.buttonLay.setBackgroundColor(getResources().getColor(R.color.green_app));
                binding.Button2nd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button3rd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button4th.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        disposable = RxView.clicks(binding.Button2nd.buttonLay).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.Button1St.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button2nd.buttonLay.setBackgroundColor(getResources().getColor(R.color.green_app));
                binding.Button3rd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button4th.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        disposable = RxView.clicks(binding.Button3rd.buttonLay).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.Button1St.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button2nd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button3rd.buttonLay.setBackgroundColor(getResources().getColor(R.color.green_app));
                binding.Button4th.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        disposable = RxView.clicks(binding.Button4th.buttonLay).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                binding.Button1St.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button2nd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button3rd.buttonLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.Button4th.buttonLay.setBackgroundColor(getResources().getColor(R.color.green_app));
            }
        });
        disposable = RxView.clicks(binding.cutQualityLay.Addcomment).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                commentDialog.show();
                bindingDialog.addCommentTxt.setText(binding.cutQualityLay.addCommentTxt.getText().toString());
                bindingDialog.save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(bindingDialog.addCommentTxt.getText().toString().isEmpty()){
                            showCustomAlert(getResources().getString(R.string.enter_comment_error));
                        }else{
                            binding.cutQualityLay.addCommentTxt.setText(bindingDialog.addCommentTxt.getText().toString());
                            commentDialog.dismiss();
                        }
                    }
                });
            }
        });
        disposable = RxView.clicks(binding.stichedQtyLay.Addcomment).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                commentDialog.show();
                bindingDialog.addCommentTxt.setText(binding.stichedQtyLay.addCommentTxt.getText().toString());
                bindingDialog.save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(bindingDialog.addCommentTxt.getText().toString().isEmpty()){
                            showCustomAlert(getResources().getString(R.string.enter_comment_error));
                        }else{
                            binding.stichedQtyLay.addCommentTxt.setText(bindingDialog.addCommentTxt.getText().toString());
                            commentDialog.dismiss();
                        }
                    }
                });
            }

        });
        disposable = RxView.clicks(binding.packedQtyLay.Addcomment).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                commentDialog.show();
                bindingDialog.addCommentTxt.setText(binding.packedQtyLay.addCommentTxt.getText().toString());
                bindingDialog.save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(bindingDialog.addCommentTxt.getText().toString().isEmpty()){
                            showCustomAlert(getResources().getString(R.string.enter_comment_error));
                        }else{
                            binding.packedQtyLay.addCommentTxt.setText(bindingDialog.addCommentTxt.getText().toString());
                            commentDialog.dismiss();
                        }
                    }
                });
            }
        });
        disposable = RxView.clicks(binding.colorRefLay.Addcomment).observeOn(AndroidSchedulers.mainThread()).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                commentDialog.show();
                bindingDialog.addCommentTxt.setText(binding.colorRefLay.addCommentTxt.getText().toString());
                bindingDialog.save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(bindingDialog.addCommentTxt.getText().toString().isEmpty()){
                            showCustomAlert(getResources().getString(R.string.enter_comment_error));
                        }else{
                            binding.colorRefLay.addCommentTxt.setText(bindingDialog.addCommentTxt.getText().toString());
                            commentDialog.dismiss();
                        }
                    }
                });
            }
        });

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    private Dialog GetCommentDialog() {
        if (commentDialog == null) {
            commentDialog = new Dialog(getContext());
            commentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commentDialog.setContentView(bindingDialog.getRoot());
            final Window window = commentDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            bindingDialog.close.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
            bindingDialog.cancel.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
        }
        return commentDialog;
    }
}