package com.kamakhya.dibzi.ui.auditDetails.survey3.otherareaaofproduct;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentOtherAreaProductBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey3.otherareaaofproduct.adapter.OtherAreaProductAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class OtherAreaProductFragment extends BaseFragment<FragmentOtherAreaProductBinding,AuditDetailViewmodel> {
private static OtherAreaProductFragment instance;
private AuditDetailViewmodel viewmodel;
private FragmentOtherAreaProductBinding binding;
private OtherAreaProductAdapter adapter;



    public OtherAreaProductFragment() {
        // Required empty public constructor
    }

    @Override
    public String toString() {
        return getString(R.string.other_area_of_product);
    }

    public static OtherAreaProductFragment newInstance() {
        instance= instance==null? new OtherAreaProductFragment():instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_other_area_product;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new OtherAreaProductAdapter(getContext());
        binding.otherAreaProduct.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}