package com.kamakhya.dibzi.ui.auditDetails.survey3.otherobservation;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentOtherObservationBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OtherObservationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OtherObservationFragment extends BaseFragment<FragmentOtherObservationBinding, AuditDetailViewmodel> {
private static OtherObservationFragment instance;
private FragmentOtherObservationBinding binding;
private AuditDetailViewmodel viewmodel;


    public OtherObservationFragment() {
        // Required empty public constructor
    }

    public static OtherObservationFragment newInstance() {
        instance =instance ==null? new OtherObservationFragment():instance;

        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_other_observation;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}