package com.kamakhya.dibzi.ui.setting.activity;

import android.app.Dialog;
import android.os.Bundle;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseActivity;
import com.kamakhya.dibzi.databinding.ActivitySettingBinding;
import com.kamakhya.dibzi.databinding.Toolbar1Binding;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.InspectionsFragment;
import com.kamakhya.dibzi.ui.setting.fragment.menu.SettingFragment;

public class SettingActivity extends BaseActivity<ActivitySettingBinding, SettingViewmodel>  {
    private ActivitySettingBinding binding;
    private SettingViewmodel viewmodel;
private int typeUpdate=0;
private String  slno="0";
private String  shipmentId="";

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    public SettingViewmodel getViewModel() {
        return viewmodel = new SettingViewmodel(this, getSharedPre(), this);
    }
    public Toolbar1Binding getToolbar(){
        return binding.toolbar;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
            if(savedInstanceState==null){
                startFragment(SettingFragment.newInstance(),SettingFragment.newInstance().toString(),true);
            }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getInternetDialog().dismiss();
        }else{
            getInternetDialog().show();
        }
    }
}