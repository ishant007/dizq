package com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentPart2PopperBinding;
import com.kamakhya.dibzi.databinding.SelectLayCustomBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail.adapter.Part1Adapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;


public class Part2PopperFragment extends BaseFragment<FragmentPart2PopperBinding,AuditDetailViewmodel> {
    private static Part2PopperFragment instance;
    private FragmentPart2PopperBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Dialog selectIonDialog;
    private SelectionDilaogAdapter dilaogAdapter;
    private SelectLayCustomBinding searchViewBinding;
    private String selction;


    public Part2PopperFragment() {
        // Required empty public constructor
    }


    public static Part2PopperFragment newInstance() {
        instance= instance ==null? new Part2PopperFragment():instance;
        return instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_part2_popper;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        searchViewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_lay_custom, null, false);
        DialogSelectReason();
        binding.fastnerSupplier.Addcomment.setImageResource(R.drawable.sliders);
        binding.fastnerSupplier2.Addcomment.setImageResource(R.drawable.sliders);
        binding.machineSupplier.Addcomment.setImageResource(R.drawable.sliders);
        binding.styleInSupplier.Addcomment.setVisibility(View.GONE);
        binding.fastnerSupplier.hederTxt.setText(getResources().getString(R.string.fastner_supplier));
        binding.fastnerSupplier2.hederTxt.setText(getResources().getString(R.string.fastner_supplier));
        binding.machineSupplier.hederTxt.setText(getResources().getString(R.string.machine_supplier));
        binding.styleInSupplier.hederTxt.setText(getResources().getString(R.string.progress_supplier));


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private void DialogSelectReason() {

        selectIonDialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        selectIonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectIonDialog.setContentView(searchViewBinding.getRoot());
        dilaogAdapter = new SelectionDilaogAdapter(getContext(), new ArrayList<>());
        searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
        searchViewBinding.closeBtn.setOnClickListener(v -> {
            selectIonDialog.dismiss();
        });
        searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIonDialog.dismiss();
            }
        });
    }
}