package com.kamakhya.dibzi.ui.auditDetails.survey1.findingandcomment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentFindingAndCommentBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey1.findingandcomment.adapter.FindingAndCommentAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

public class FindingAndCommentFragment extends BaseFragment<FragmentFindingAndCommentBinding,AuditDetailViewmodel> {
    private static FindingAndCommentFragment instance;
    private FragmentFindingAndCommentBinding binding;
    private AuditDetailViewmodel viewmodel;
    private FindingAndCommentAdapter adapter;

    public FindingAndCommentFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public String toString() {
        return FindingAndCommentFragment.class.getName();
    }

    public static FindingAndCommentFragment newInstance() {
        instance = instance == null ? new FindingAndCommentFragment() : instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_finding_and_comment;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new FindingAndCommentAdapter(getContext());
        binding.commentAndFindingRecycler.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}