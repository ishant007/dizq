package com.kamakhya.dibzi.ui.schedules.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.ScheduleItemBinding;
import com.kamakhya.dibzi.retrofitModel.getSchedule.ResultsItem;
import com.kamakhya.dibzi.setting.AppConstance;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.auditDetails.AuditDetailsActivity;
import com.kamakhya.dibzi.ui.schedules.ScheduleNav;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {
    private Context context;
    private ScheduleNav scheduleNav;
    private List<ResultsItem> schedules =new ArrayList<>();

    public ScheduleAdapter(Context context, ScheduleNav scheduleNav) {
        this.context = context;
        this.scheduleNav = scheduleNav;
    }

    public void UpdateList(List<ResultsItem> schedules) {
        this.schedules=schedules;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ScheduleItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.schedule_item, parent, false);
        return new ScheduleViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleViewHolder holder, final int position) {
        holder.ViewHolderConnection(schedules.get(position),position);
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public void removeFromList(int pos) {
        schedules.remove(pos);
        notifyDataSetChanged();
    }

    class ScheduleViewHolder extends RecyclerView.ViewHolder {
        private ScheduleItemBinding binding;

        public ScheduleViewHolder(@NonNull ScheduleItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void ViewHolderConnection(final ResultsItem schedule, final int position) {
            if(schedule.getInspectionType().equalsIgnoreCase("Inline inspection")){
                schedule.setScheduleType(AppConstance.INCLINE_INCEPTION);
            }else if(schedule.getInspectionType().equalsIgnoreCase("Gold seal sample")){
                schedule.setScheduleType(AppConstance.GOLD_SEAL_SAMPLE);
            }else if(schedule.getInspectionType().equalsIgnoreCase("Weekly Safly Inspection")){
                schedule.setScheduleType(AppConstance.WEEKLY_SAFETY);
            }else if(schedule.getInspectionType().equalsIgnoreCase("Pilot/First Off Production Check List")){
                schedule.setScheduleType(AppConstance.PILOT_PRODUCTION_CHECKLIST);
            }else if(schedule.getInspectionType().equalsIgnoreCase("Pre-Final Inspections")){
                schedule.setScheduleType(AppConstance.PRE_FINAL_INSPECTION);
            }else{
                schedule.setScheduleType(0);
            }
            binding.auditorIncharge.setText(schedule.getAuditor());
            binding.factoryName.setText(schedule.getFactoryName());
            binding.styleCode.setText("");
            binding.auditType.setText(schedule.getInspectionType());
            binding.dateSchedules.setText(CommonUtils.getFormattedDate(schedule.getDate(),"MM/dd/yyyy hh:mm:ss aa","h:mm aa"));
            binding.editSchedule.setOnClickListener(v -> {
                scheduleNav.ClickOnEditButton(schedule,position);
            });
            binding.checkScheduleDetail.setOnClickListener(v ->
            {
                scheduleNav.ClickOnScheduleDetail(schedule,position);
            });

            binding.deleteSchedule.setOnClickListener(v ->
            {
                scheduleNav.ClickOnDeleteScheduleDetail(schedule,position);
            });
        }
    }
}
