package com.kamakhya.dibzi.ui.auditDetails.survey2.labeling;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.CommentDilaogLayBinding;
import com.kamakhya.dibzi.databinding.FragmentLabelingBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

public class LabelingFragment extends BaseFragment<FragmentLabelingBinding,AuditDetailViewmodel> {
private static LabelingFragment instance;
private FragmentLabelingBinding binding;
private AuditDetailViewmodel viewmodel;
    private Dialog commentDialog;
    private CommentDilaogLayBinding bindingDialog;
    public LabelingFragment() {
        // Required empty public constructor
    }


    public static LabelingFragment newInstance() {
        instance= instance==null ?new LabelingFragment():instance;
        return instance;
    }

    @Override
    public String toString() {
        return LabelingFragment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_labeling;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.comment_dilaog_lay, null, false);
        GetCommentDialog();
        DesigningSetup();
        clickListners();
    }
    private void DesigningSetup() {
        binding.careLabelLay.HeaderName.setText(getResources().getString(R.string.care_label));
        binding.dataPinLabelLay.HeaderName.setText(getResources().getString(R.string.dat_pip));
        binding.mainLabelLay.HeaderName.setText(getResources().getString(R.string.main_label));
        binding.patchLabelLay.HeaderName.setText(getResources().getString(R.string.patch_label));
        binding.sizeLabelLay.HeaderName.setText(getResources().getString(R.string.size_label));
        //correctBtn
        binding.careLabelLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.dataPinLabelLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.mainLabelLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.patchLabelLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        binding.sizeLabelLay.correctBtn.buttonHeader.setText(getResources().getString(R.string.correct));
        //incorreect
        binding.careLabelLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.dataPinLabelLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.mainLabelLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.patchLabelLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        binding.sizeLabelLay.incorrenctBtn.buttonHeader.setText(getResources().getString(R.string.incorrect));
        //not acceptable
        binding.careLabelLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.dataPinLabelLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.mainLabelLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.patchLabelLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
        binding.sizeLabelLay.notAcceptableBtn.buttonHeader.setText(getResources().getString(R.string.not_acceptable));
    }
    private void clickListners() {
        binding.careLabelLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();

            }
        });
        binding.dataPinLabelLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.mainLabelLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.patchLabelLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
        binding.sizeLabelLay.Addcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentDialog.show();
            }
        });
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private Dialog GetCommentDialog() {
        if (commentDialog == null) {
            commentDialog = new Dialog(getContext());
            commentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commentDialog.setContentView(bindingDialog.getRoot());
            final Window window = commentDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.CENTER);
            bindingDialog.close.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
            bindingDialog.cancel.setOnClickListener(v -> {
                bindingDialog.highlight.setChecked(false);
                bindingDialog.addCommentTxt.setText("");
                commentDialog.dismiss();
            });
        }
        return commentDialog;
    }
}