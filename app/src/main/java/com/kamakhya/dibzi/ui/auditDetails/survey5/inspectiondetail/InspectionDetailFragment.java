package com.kamakhya.dibzi.ui.auditDetails.survey5.inspectiondetail;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentInspectionDetailBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class InspectionDetailFragment extends BaseFragment<FragmentInspectionDetailBinding,AuditDetailViewmodel> {
    private static InspectionDetailFragment instance;
    private FragmentInspectionDetailBinding binding;
    private AuditDetailViewmodel viewmodel;

    public InspectionDetailFragment() {
        // Required empty public constructor
    }


    public static InspectionDetailFragment newInstance() {
        instance= instance ==null? new InspectionDetailFragment():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_inspection_detail;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}