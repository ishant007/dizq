package com.kamakhya.dibzi.ui.setting.activity;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
import android.app.Activity;
import android.content.Context;

import androidx.constraintlayout.solver.state.helpers.AlignVerticallyReference;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kamakhya.dibzi.baseclasses.BaseViewModel;
import com.kamakhya.dibzi.prefrences.SharedPre;
import com.kamakhya.dibzi.retrofitModel.createSchedule.CreateScheduleResponse;
import com.kamakhya.dibzi.retrofitModel.getAllShipmentIds.GetAllShipmentIdsResponse;
import com.kamakhya.dibzi.retrofitModel.getAllShipmentIds.ResultsItem;
import com.kamakhya.dibzi.retrofitModel.getAuditorsData.AuditorResponse;
import com.kamakhya.dibzi.retrofitModel.getDataByShipmentId.ShipmentIdDataResponse;
import com.kamakhya.dibzi.retrofitModel.getPoDetails.GetPoDetailsResponse;
import com.kamakhya.dibzi.retrofitModel.getPoNumber.GetPoNumberResponse;
import com.kamakhya.dibzi.retrofitModel.getauditlist.AuditListResponse;
import com.kamakhya.dibzi.retrofitModel.login.Response;
import com.kamakhya.dibzi.retrofitModel.updateSchedule.UpdateScheduleResponse;
import com.kamakhya.dibzi.setting.AppConstance;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SettingViewmodel extends BaseViewModel<SettingNav> {
    private MutableLiveData<List<ResultsItem>> getAllShipmentIds=new MutableLiveData<>();
    public SettingViewmodel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }


    public void CreateRequest(long slNo ,String date,int comapnyId ,String auditType,long poslNo ,
                              String auditor,String customer ,String factoryName,String garmentVendore,String productCatogry,
                              String styleCode, String discription,Double qty,String poNumber){
        getNavigator().showLoading(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SlNo", slNo);
            jsonObject.put("ScheduleDate", date);
            jsonObject.put("CompanyId", comapnyId);
            jsonObject.put("AuditType", auditType);
            jsonObject.put("POSlNo", poslNo);
            jsonObject.put("Auditor", auditor);
            jsonObject.put("Customer", customer);
            jsonObject.put("Factory", factoryName);
            jsonObject.put("GarmentVendor", garmentVendore);
            jsonObject.put("ProductCategory", productCatogry);
            jsonObject.put("StyleCode", styleCode);
            jsonObject.put("Description", discription);
            jsonObject.put("OrderedQuantity", qty);
            jsonObject.put("PONumber", poNumber);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppConstance.BASE_URL+AppConstance.CREATE_SCHEDULE)
                .addJSONObjectBody(jsonObject) // posting json
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(CreateScheduleResponse.class, new ParsedRequestListener<CreateScheduleResponse>() {
                    @Override
                    public void onResponse(CreateScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getResults().getReturnStatus().equalsIgnoreCase("Error")){
                            getNavigator().showMessage(response.getResults().getReturnMessage());
                        }else{
                            getNavigator().showMessage(response.getResults().getReturnMessage());
                            getNavigator().ScheduleCreated();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                    }
                });
    }

    public void CreateScheduleByShipment(long slNo,String shipmentId ,String date,int comapnyId ,String auditType,String poNumber ,
                              String auditor,String factoryName,String buyerPoNumber,String garmentVendore,String productCatogry,
                              String styleCode,Double qty){
        getNavigator().showLoading(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SlNo", slNo);
            jsonObject.put("ScheduleDate", date);
            jsonObject.put("CompanyId", comapnyId);
            jsonObject.put("AuditType", auditType);
            jsonObject.put("ShipmentId", shipmentId);
            jsonObject.put("Auditor", auditor);
            jsonObject.put("BuyerPoNo", buyerPoNumber);
            jsonObject.put("Factory", factoryName);
            jsonObject.put("Supplier", garmentVendore);
            jsonObject.put("ProductCategory", productCatogry);
            jsonObject.put("StyleCode", styleCode);
            jsonObject.put("PONumber", poNumber);
            jsonObject.put("OfferedQuantity", qty);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppConstance.BASE_URL+AppConstance.CREATE_SCHEDULE_BY_SHIPMENT)
                .addJSONObjectBody(jsonObject) // posting json
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(CreateScheduleResponse.class, new ParsedRequestListener<CreateScheduleResponse>() {
                    @Override
                    public void onResponse(CreateScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getResults().getReturnStatus().equalsIgnoreCase("Error")){
                            getNavigator().showMessage(response.getResults().getReturnMessage());
                        }else{
                            getNavigator().showMessage(response.getResults().getReturnMessage());
                            getNavigator().ScheduleCreated();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                    }
                });
    }
    public void CallAllGarmentsPo() {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.GET_PO_GARMENTS)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetPoNumberResponse.class, new ParsedRequestListener<GetPoNumberResponse>() {
                    @Override
                    public void onResponse(GetPoNumberResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null){
                            getNavigator().UpdateGarmentPO(response.getResults());
                        }else{
                            getNavigator().Logout();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void CallDataShipmentId(String shipmentId) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.GET_PO_BY_SHIPMENT_ID)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .addQueryParameter("shipmentId",shipmentId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(ShipmentIdDataResponse.class, new ParsedRequestListener<ShipmentIdDataResponse>() {
                    @Override
                    public void onResponse(ShipmentIdDataResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null ){
                            getNavigator().GetDataByShipmentId(response.getResult());
                        }else{
                            getNavigator().Logout();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void callPoGarmentdetail(String poslNumber) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.GET_PO_GARMENTS_DETAIL)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .addQueryParameter("poSlNo",poslNumber)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetPoDetailsResponse.class, new ParsedRequestListener<GetPoDetailsResponse>() {
                    @Override
                    public void onResponse(GetPoDetailsResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null ){
                            getNavigator().UpdateGarmentsDetails(response.getResult());
                        }else{
                            getNavigator().Logout();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void getAuditorData() {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.AUDITORS)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(AuditorResponse.class, new ParsedRequestListener<AuditorResponse>() {
                    @Override
                    public void onResponse(AuditorResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null ){
                            getNavigator().AuditorData(response.getResults());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void getAuditListData() {
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.AUDIT_LIST)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(AuditListResponse.class, new ParsedRequestListener<AuditListResponse>() {
                    @Override
                    public void onResponse(AuditListResponse response) {

                        if(response!=null ){
                            getNavigator().AuditListData(response.getResults());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    private MutableLiveData<List<ResultsItem>> GetAllShipmentIds(){

        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.SHIPMENT_IDS)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetAllShipmentIdsResponse.class, new ParsedRequestListener<GetAllShipmentIdsResponse>() {
                    @Override
                    public void onResponse(GetAllShipmentIdsResponse response) {
                        if(response!=null ){
                            getAllShipmentIds.postValue(response.getResults());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
      return   getAllShipmentIds;
    }

    public LiveData<List<ResultsItem>>GetAllShipments(){
        return GetAllShipmentIds();
    }

    public void UpdateDataListByShipment(String slno){
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.UPDATE_DATA_SHIPMENT_IDS)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .addQueryParameter(AppConstance.SLNO,slno)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(ShipmentIdDataResponse.class, new ParsedRequestListener<ShipmentIdDataResponse>() {
                    @Override
                    public void onResponse(ShipmentIdDataResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null ){
                            getNavigator().GetDataByShipmentId(response.getResult());
                        }else{
                            getNavigator().Logout();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void UpdateDataList(String slno) {
        getNavigator().showLoading(true);
        AndroidNetworking.get(AppConstance.BASE_URL+AppConstance.UPDATE_DATA_SHIPMENT_IDS)
                .addHeaders(AppConstance.AUTHORIZE_TOKEN,getSharedPre().getUserToken())
                .addQueryParameter(AppConstance.SLNO,slno)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(UpdateScheduleResponse.class, new ParsedRequestListener<UpdateScheduleResponse>() {
                    @Override
                    public void onResponse(UpdateScheduleResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null ){
                            getNavigator().UpdateSchedule(response.getResult());
                        }else{
                            getNavigator().Logout();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
}
