package com.kamakhya.dibzi.ui.auditDetails.survey2.inHouseWash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentFindingAndCommentBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.inHouseWash.adapter.InHouseWashAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

public class InHouseWashFragment extends BaseFragment<FragmentFindingAndCommentBinding,AuditDetailViewmodel> {
    private static InHouseWashFragment instance;
    private FragmentFindingAndCommentBinding binding;
    private AuditDetailViewmodel viewmodel;
    private InHouseWashAdapter adapter;

    public InHouseWashFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public String toString() {
        return InHouseWashFragment.class.getName();
    }

    public static InHouseWashFragment newInstance() {
        instance = instance == null ? new InHouseWashFragment() : instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_finding_and_comment;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        adapter=new InHouseWashAdapter(getContext());
        binding.commentAndFindingRecycler.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity(). getInternetDialog().show();
        }
    }
}