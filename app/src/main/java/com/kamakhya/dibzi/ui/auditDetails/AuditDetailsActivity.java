package com.kamakhya.dibzi.ui.auditDetails;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseActivity;
import com.kamakhya.dibzi.databinding.ActivityAuditDetailsBinding;
import com.kamakhya.dibzi.retrofitModel.getSchedule.ResultsItem;
import com.kamakhya.dibzi.ui.auditDetails.Adapter.ViewPagerFragmentAdapter;
import com.kamakhya.dibzi.ui.auditDetails.survey1.comments.CommentsFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.factorycontactdetails.FactoryContactDetailFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.finalstatus.FinalStatusFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.findingandcomment.FindingAndCommentFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.genral.GeneralFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.inspection.InspectionFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementdiscri.MeasurementDiscrepanciesFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.mesurementreport.MeasurementReportFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey1.submit.SubmitFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.approval.ApprovalFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.asseorieslabel.AccessoriesFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.fabricAndWashing.FabricAndWashingFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.fabrictrim.FabricTrim;
import com.kamakhya.dibzi.ui.auditDetails.survey2.fitting.FittingFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.inHouseWash.InHouseWashFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.labeling.LabelingFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.making.MakingFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.SampleDetailFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sizespecification.SizeSpecificationFragmnet;
import com.kamakhya.dibzi.ui.auditDetails.survey2.workmanship.WorkmanShipFragmnet;
import com.kamakhya.dibzi.ui.auditDetails.survey3.finalStatus.FinalStatus;
import com.kamakhya.dibzi.ui.auditDetails.survey3.healthandsaftey.HealthAndSafetyFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.metaldetector.MetalDetectorFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.otherareaaofproduct.OtherAreaProductFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.otherobservation.OtherObservationFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part1detail.Part1DetailFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier.Part2DetailFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier.Part2FastnerFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier.Part2PopperFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.part2poppersupplier.Part2StyleFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.productsaftey.ProductSafetyFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey3.quality.QualityFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.aprovedtrimfabric.ApprovedFabricTrim;
import com.kamakhya.dibzi.ui.auditDetails.survey4.creticaloperation.CreticalOperationFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.event.EventFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.finalstatusfragment.FinalStausSurvey4Fragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.mesaurementcreticalpoint.MeasurementCreticalPointFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.operationfragment.OperationFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey4.pilot1offdatafragment.Pilot1StOffDataFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliance.ComplianceCheckHangingFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancecheckdebox.ComplianceDebox;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancecheckedbox.ComplianceCheckedBoxFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.complianceckeckother.ComplianceCheckOther;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.Compliance;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.FabricPrintingFinishing;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.Interlining;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.MakeUp;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.MetalContamination;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.Presentation;
import com.kamakhya.dibzi.ui.auditDetails.survey5.compliancequestion.fragments.TrimsComponents;
import com.kamakhya.dibzi.ui.auditDetails.survey5.finalstatus.FinalStatusSurvey5Fragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.inspectiondetail.InspectionDetailFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.measurement.CertificateFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.measurement.MeasurementFragment;
import com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.ProductPicture;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailNav;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import static com.kamakhya.dibzi.setting.AppConstance.GOLD_SEAL_SAMPLE;
import static com.kamakhya.dibzi.setting.AppConstance.INCLINE_INCEPTION;
import static com.kamakhya.dibzi.setting.AppConstance.PILOT_PRODUCTION_CHECKLIST;
import static com.kamakhya.dibzi.setting.AppConstance.PRE_FINAL_INSPECTION;
import static com.kamakhya.dibzi.setting.AppConstance.WEEKLY_SAFETY;

public class AuditDetailsActivity extends BaseActivity<ActivityAuditDetailsBinding, AuditDetailViewmodel> implements AuditDetailNav {
    private ActivityAuditDetailsBinding binding;
    private AuditDetailViewmodel viewmodel;
    private Dialog internetDialog;
    private String[] tabs = new String[]{};
    private ViewPagerFragmentAdapter adapter;
    private ResultsItem schedule;
    private int pageView=3;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_audit_details;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            binding = getViewDataBinding();
            viewmodel.setNavigator(this);
            schedule=new Gson().fromJson(getIntent().getStringExtra("schedule"), new TypeToken<ResultsItem>() {
            }.getType());
            if(schedule==null){
                onBackPressed();
            }
            pageView=schedule.getScheduleType();
            adapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), 0);
            binding.toolbar.factoryname.setText(schedule.getFactoryName());
            if (pageView == 0) {
                showCustomAlert("No Data Found");
            }else{
                switch(pageView){
                    case INCLINE_INCEPTION:{
                        binding.toolbar.heder.setText(schedule.getInspectionType());
                        adapter.addFragment(GeneralFragment.newInstance(INCLINE_INCEPTION), getResources().getString(R.string.genral));
                        adapter.addFragment(InspectionFragment.newInstance(), getResources().getString(R.string.inspection));
                        adapter.addFragment(FindingAndCommentFragment.newInstance(), getResources().getString(R.string.finding_comment__inline));
                        adapter.addFragment(MeasurementDiscrepanciesFragment.newInstance(), getResources().getString(R.string.summery_measurement));
                        adapter.addFragment(MeasurementReportFragment.newInstance(), getResources().getString(R.string.measurement_report));
                        adapter.addFragment(FinalStatusFragment.newInstance(), getResources().getString(R.string.final_status));
                        adapter.addFragment(CommentsFragment.newInstance(), getResources().getString(R.string.comments));
                        adapter.addFragment(FactoryContactDetailFragment.newInstance(), getResources().getString(R.string.factory_cotact_details));
                        adapter.addFragment(SubmitFragment.newInstance(), getResources().getString(R.string.submit));
                        binding.viewpagerOfAudit.setAdapter(adapter);
                        break;
                    }
                    case GOLD_SEAL_SAMPLE:{
                        binding.toolbar.heder.setText(schedule.getInspectionType());
                        adapter.addFragment(GeneralFragment.newInstance(GOLD_SEAL_SAMPLE), getResources().getString(R.string.genral));
                        adapter.addFragment(SampleDetailFragment.newInstance(), getResources().getString(R.string.sample_detail));
                        adapter.addFragment(FabricTrim.newInstance(), getResources().getString(R.string.fabric_trims));
                        adapter.addFragment(MakingFragment.newInstance(), getResources().getString(R.string.making));
                        adapter.addFragment(LabelingFragment.newInstance(), getResources().getString(R.string.labeling));
                        adapter.addFragment(FittingFragment.newInstance(), getResources().getString(R.string.fitting));
                        adapter.addFragment(SizeSpecificationFragmnet.newInstance(), getResources().getString(R.string.sizespecification));
                        adapter.addFragment(WorkmanShipFragmnet.newInstance(), getResources().getString(R.string.workmanship));
                        adapter.addFragment(AccessoriesFragment.newInstance(), getResources().getString(R.string.assesoriesandlabel));
                        adapter.addFragment(FabricAndWashingFragment.newInstance(), getResources().getString(R.string.fabric_washing));
                        adapter.addFragment(InHouseWashFragment.newInstance(), getResources().getString(R.string.inhouse_wash));
                        adapter.addFragment(ApprovalFragment.newInstance(), getResources().getString(R.string.approval));
                        adapter.addFragment(CommentsFragment.newInstance(), getResources().getString(R.string.comments));
                        adapter.addFragment(FactoryContactDetailFragment.newInstance(), getResources().getString(R.string.factory_cotact_details));
                        adapter.addFragment(SubmitFragment.newInstance(), getResources().getString(R.string.submit));
                        binding.viewpagerOfAudit.setAdapter(adapter);
                        break;

                    }
                    case WEEKLY_SAFETY:{
                        binding.toolbar.heder.setText(schedule.getInspectionType());
                        adapter.addFragment(GeneralFragment.newInstance(WEEKLY_SAFETY), getResources().getString(R.string.genral));
                        adapter.addFragment(ProductSafetyFragment.newInstance(), getResources().getString(R.string.product_safety));
                        adapter.addFragment(QualityFragment.newInstance(), getResources().getString(R.string.quality));
                        adapter.addFragment(HealthAndSafetyFragment.newInstance(), getResources().getString(R.string.health_and_safety));
                        adapter.addFragment(MetalDetectorFragment.newInstance(), getResources().getString(R.string.metal_detector));
                        adapter.addFragment(OtherAreaProductFragment.newInstance(), getResources().getString(R.string.other_area_of_product));
                        adapter.addFragment(Part1DetailFragment.newInstance(), getResources().getString(R.string.part_1));
                        adapter.addFragment(Part2PopperFragment.newInstance(), getResources().getString(R.string.popper_supplier_frag));
                        adapter.addFragment(Part2FastnerFragment.newInstance(), getResources().getString(R.string.popper_fastner_frag));
                        adapter.addFragment(Part2StyleFragment.newInstance(), getResources().getString(R.string.popper_styyler_part2));
                        adapter.addFragment(Part2DetailFragment.newInstance(), getResources().getString(R.string.detail_part2_frag));
                        adapter.addFragment(OtherObservationFragment.newInstance(), getResources().getString(R.string.other_observation));
                        adapter.addFragment(FinalStatus.newInstance(), getResources().getString(R.string.final_status));
                        adapter.addFragment(CommentsFragment.newInstance(), getResources().getString(R.string.comments));
                        adapter.addFragment(FactoryContactDetailFragment.newInstance(), getResources().getString(R.string.factory_cotact_details));
                        adapter.addFragment(SubmitFragment.newInstance(), getResources().getString(R.string.submit));
                        binding.viewpagerOfAudit.setAdapter(adapter);
                        break;
                    }
                    case PILOT_PRODUCTION_CHECKLIST:{
                        binding.toolbar.heder.setText(schedule.getInspectionType());
                        adapter.addFragment(GeneralFragment.newInstance(PILOT_PRODUCTION_CHECKLIST), getResources().getString(R.string.genral));
                        adapter.addFragment(Pilot1StOffDataFragment.newInstance(), getResources().getString(R.string.pilot_1st_of_data));
                        adapter.addFragment(ApprovedFabricTrim.newInstance(), getResources().getString(R.string.approved_trim));
                        adapter.addFragment(OperationFragment.newInstance(), getResources().getString(R.string.operation));
                        adapter.addFragment(CreticalOperationFragment.newInstance(), getResources().getString(R.string.cretical_operation));
                        adapter.addFragment(MeasurementCreticalPointFragment.newInstance(), getResources().getString(R.string.measurement_cretical_point));
                        adapter.addFragment(EventFragment.newInstance(), getResources().getString(R.string.event));
                        adapter.addFragment(OtherObservationFragment.newInstance(), getResources().getString(R.string.other_observation));
                        adapter.addFragment(FinalStausSurvey4Fragment.newInstance(), getResources().getString(R.string.final_status));
                        adapter.addFragment(CommentsFragment.newInstance(), getResources().getString(R.string.comments));
                        adapter.addFragment(FactoryContactDetailFragment.newInstance(), getResources().getString(R.string.factory_cotact_details));
                        adapter.addFragment(SubmitFragment.newInstance(), getResources().getString(R.string.submit));
                        binding.viewpagerOfAudit.setAdapter(adapter);
                        break;
                    }
                    case PRE_FINAL_INSPECTION:{
                        binding.toolbar.heder.setText(schedule.getInspectionType());
                        adapter.addFragment(GeneralFragment.newInstance(PRE_FINAL_INSPECTION), getResources().getString(R.string.genral));
                        adapter.addFragment(ProductPicture.newInstance(), getResources().getString(R.string.productPicture));
                        adapter.addFragment(InspectionDetailFragment.newInstance(), getResources().getString(R.string.inspection_detail));
                        adapter.addFragment(ComplianceCheckedBoxFragment.newInstance(), getResources().getString(R.string.compliance_checkedBox));
                        adapter.addFragment(ComplianceCheckHangingFragment.newInstance(), getResources().getString(R.string.compliance_checkedHAnging));
                        adapter.addFragment(ComplianceDebox.newInstance(), getResources().getString(R.string.compliance_checked_debox));
                        adapter.addFragment(ComplianceCheckOther.newInstance(), getResources().getString(R.string.compliance_checked_debox));
                        adapter.addFragment(Compliance.newInstance(), getResources().getString(R.string.compliance));
                        adapter.addFragment(Presentation.newInstance(), getResources().getString(R.string.presentation));
                        adapter.addFragment(MetalContamination.newInstance(), getResources().getString(R.string.metal_contamination));
                        adapter.addFragment(FabricPrintingFinishing.newInstance(), getResources().getString(R.string.fabric_printing_finishing));
                        adapter.addFragment(Interlining.newInstance(), getResources().getString(R.string.interlining));
                        adapter.addFragment(MakeUp.newInstance(), getResources().getString(R.string.make_up));
                        adapter.addFragment(TrimsComponents.newInstance(), getResources().getString(R.string.trims_components));
                        adapter.addFragment(MeasurementFragment.newInstance(), getResources().getString(R.string.measurements_big));
                        adapter.addFragment(CertificateFragment.newInstance(), getResources().getString(R.string.certificate));
                        adapter.addFragment(OtherObservationFragment.newInstance(), getResources().getString(R.string.other_observation));
                        adapter.addFragment(FinalStatusSurvey5Fragment.newInstance(), getResources().getString(R.string.final_status));
                        adapter.addFragment(CommentsFragment.newInstance(), getResources().getString(R.string.comments));
                        adapter.addFragment(FactoryContactDetailFragment.newInstance(), getResources().getString(R.string.factory_cotact_details));
                        adapter.addFragment(SubmitFragment.newInstance(), getResources().getString(R.string.submit));
                        binding.viewpagerOfAudit.setAdapter(adapter);
                        break;
                    }
                    default:{
                        showCustomAlert("No Data Found Please Try Again");
                    }
                }
            }
            binding.tabs.setupWithViewPager(binding.viewpagerOfAudit);
            binding.toolbar.closeBtn.setOnClickListener(v -> onBackPressed());
        }
        catch (Exception e){
            showCustomAlert("Please Restart App");
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();
        } else {
            getInternetDialog().show();
        }
    }
}