package com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.SelcetionDialogItemBinding;

import java.util.ArrayList;
import java.util.List;

public class SelectionDilaogAdapter extends RecyclerView.Adapter<SelectionDilaogAdapter.SelectionDilaogViewholder> {
    private Context context;
    private List<String> selectionElements = new ArrayList<>();
    private List<String> selected = new ArrayList<>();
    private int postion = 0;

    public SelectionDilaogAdapter(Context context, List<String> selectionElements) {
        this.context = context;
        this.selectionElements = selectionElements;
    }


    @NonNull
    @Override
    public SelectionDilaogViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SelcetionDialogItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.selcetion_dialog_item, parent, false);
        return new SelectionDilaogViewholder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectionDilaogViewholder holder, final int position) {
        holder.BindItem(selectionElements.get(position), position);

    }
    public List<String>getSelectedString(){
        if(selected.size()>0){
            return selected;
        }else{
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return selectionElements.size();
    }

    public void UpdateList(List<String> listSelection) {
        this.selectionElements=listSelection;
        selected.clear();
        notifyDataSetChanged();
    }

    class SelectionDilaogViewholder extends RecyclerView.ViewHolder {
        private SelcetionDialogItemBinding binding;

        public SelectionDilaogViewholder(@NonNull SelcetionDialogItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void BindItem(final String s, final int position) {
            binding.txtSaftey.setText(s);
            binding.saftyCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!binding.saftyCheck.isChecked()) {
                        for (int i = 0; i < selected.size(); i++) {
                            if (selected.get(i).equalsIgnoreCase(s)) {
                                selected.remove(i);
                            }
                        }

                    } else {
                        selected.add(s);
                    }
                }
            });
        }
    }
}
