package com.kamakhya.dibzi.ui.auditDetails.survey1.finalstatus;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentFinalStatusBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class FinalStatusFragment extends BaseFragment<FragmentFinalStatusBinding,AuditDetailViewmodel> {
    private static FinalStatusFragment instance;
    private FragmentFinalStatusBinding binding;
    private AuditDetailViewmodel viewmodel;

    public FinalStatusFragment() {

    }

    @NonNull
    @Override
    public String toString() {
        return FinalStatusFragment.class.getName();
    }

    public static FinalStatusFragment newInstance() {
        instance= instance ==null? new FinalStatusFragment():instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_final_status;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.fail.buttonHeader.setText(R.string.fail);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}