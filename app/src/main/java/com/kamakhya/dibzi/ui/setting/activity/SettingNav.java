package com.kamakhya.dibzi.ui.setting.activity;

import com.kamakhya.dibzi.retrofitModel.getPoDetails.Result;
import com.kamakhya.dibzi.retrofitModel.getPoNumber.ResultsItem;

import java.util.List;

public interface SettingNav {
    void showLoading(boolean b);

    void UpdateGarmentPO(List<ResultsItem> results);

    void Logout();

    void showMessage(String message);

    void UpdateGarmentsDetails(Result result);


    void AuditorData(List<com.kamakhya.dibzi.retrofitModel.getAuditorsData.ResultsItem> results);

    void AuditListData(List<com.kamakhya.dibzi.retrofitModel.getauditlist.ResultsItem> results);

    void GetDataByShipmentId(com.kamakhya.dibzi.retrofitModel.getDataByShipmentId.Result result);

    void ScheduleCreated();

    void UpdateSchedule(com.kamakhya.dibzi.retrofitModel.updateSchedule.Result result);
}
