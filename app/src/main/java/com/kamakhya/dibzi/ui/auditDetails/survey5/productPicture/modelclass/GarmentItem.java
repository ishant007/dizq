package com.kamakhya.dibzi.ui.auditDetails.survey5.productPicture.modelclass;

public class GarmentItem {
    private int id;
    private String urlOfPicture;

    public GarmentItem(int id, String urlOfPicture) {
        this.id = id;
        this.urlOfPicture = urlOfPicture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlOfPicture() {
        return urlOfPicture;
    }

    public void setUrlOfPicture(String urlOfPicture) {
        this.urlOfPicture = urlOfPicture;
    }
}
