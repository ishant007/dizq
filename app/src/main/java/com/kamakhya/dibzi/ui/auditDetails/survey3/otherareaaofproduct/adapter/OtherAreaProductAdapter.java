package com.kamakhya.dibzi.ui.auditDetails.survey3.otherareaaofproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.databinding.OtherAreaProductItemBinding;

public class OtherAreaProductAdapter extends RecyclerView.Adapter<OtherAreaProductAdapter.OtherAreaViewHolder> {
    private Context context;

    public OtherAreaProductAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public OtherAreaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OtherAreaProductItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.other_area_product_item,parent,false);
        return new OtherAreaViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OtherAreaViewHolder holder, int position) {
        holder.OnBind(position);
    }

    @Override
    public int getItemCount() {
        return 2;
    }


    class OtherAreaViewHolder extends RecyclerView.ViewHolder {
        private OtherAreaProductItemBinding binding;
        public OtherAreaViewHolder(@NonNull OtherAreaProductItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void OnBind(final int position) {
        }
    }
}
