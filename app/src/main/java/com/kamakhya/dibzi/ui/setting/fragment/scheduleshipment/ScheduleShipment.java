package com.kamakhya.dibzi.ui.setting.fragment.scheduleshipment;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxbinding3.view.RxView;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentScheduleShipmentBinding;
import com.kamakhya.dibzi.retrofitModel.getAllShipmentIds.ResultsItem;
import com.kamakhya.dibzi.setting.AppConstance;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.schedules.SchedulesActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.setting.activity.SettingViewmodel;
import com.kamakhya.dibzi.ui.setting.fragment.inspections.InspectionsFragment;
import com.kamakhya.dibzi.ui.setting.fragment.scheduleshipment.adapter.ScheduleShipmentAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.Unit;


public class ScheduleShipment extends BaseFragment<FragmentScheduleShipmentBinding, SettingViewmodel> {
    private static ScheduleShipment instance;
    private FragmentScheduleShipmentBinding binding;
    private SettingViewmodel viewmodel;
    private Dialog internetDialog;
    private Disposable disposable;
    private ScheduleShipmentAdapter adapter;
    private List<ResultsItem> resultsItems =new ArrayList<>();


    public ScheduleShipment() {
        // Required empty public constructor
    }


    public static ScheduleShipment newInstance() {
        instance = instance == null ? new ScheduleShipment() : instance;
        return instance;
    }

    @Override
    public String toString() {
        return ScheduleShipment.class.getName();
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_schedule_shipment;
    }

    @Override
    public SettingViewmodel getViewModel() {
        return viewmodel = new SettingViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    private void onClisckListners() {
        ((SettingActivity) getBaseActivity()).getToolbar().title.setText(getString(R.string.schedule_shipment));
        ((SettingActivity) getBaseActivity()).getToolbar().closeBtn.setVisibility(View.VISIBLE);
        ((SettingActivity) getBaseActivity()).getToolbar().saveBtn.setVisibility(View.INVISIBLE);
        disposable = RxView.clicks(binding.getShipmentbtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                if (binding.shipmentId.getText().toString().trim().isEmpty()) {
                    showCustomAlert("Please Enter Shipment Id");
                } else {
                    getBaseActivity().startFragment(InspectionsFragment.newInstance(binding.shipmentId.getText().toString().trim(), "0"), true, InspectionsFragment.newInstance(binding.shipmentId.getText().toString().trim(), "0").toString());
                }
            }
        });
        disposable = RxView.clicks(((SettingActivity) getBaseActivity()).getToolbar().closeBtn).observeOn(AndroidSchedulers.mainThread()).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Exception {
                getBaseActivity().onBackPressed();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();

        onClisckListners();
        adapter=new ScheduleShipmentAdapter(getContext(),resultsItems);
        viewmodel.GetAllShipments().observe(getBaseActivity(), new Observer<List<ResultsItem>>() {
            @Override
            public void onChanged(List<ResultsItem> resultsItemsMain) {
                resultsItems=resultsItemsMain;
                adapter.UpdateList(resultsItems);
            }
        });
        binding.shipmentIdsRecyler.setAdapter(adapter);
        binding.shipmentIdsRecyler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        try {
            if (isConnected) {
                getBaseActivity().getInternetDialog().dismiss();
            } else {
                getBaseActivity().getInternetDialog().show();
            }

        }catch (Exception e){

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((SettingActivity) getBaseActivity()).getToolbar().closeBtn.setVisibility(View.INVISIBLE);
        ((SettingActivity) getBaseActivity()).getToolbar().saveBtn.setVisibility(View.INVISIBLE);
    }
}