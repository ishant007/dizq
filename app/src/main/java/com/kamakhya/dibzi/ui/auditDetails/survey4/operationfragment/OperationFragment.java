package com.kamakhya.dibzi.ui.auditDetails.survey4.operationfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentOperationBinding;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;


public class OperationFragment extends BaseFragment<FragmentOperationBinding,AuditDetailViewmodel> {
private static OperationFragment fragment;
private FragmentOperationBinding binding;
private AuditDetailViewmodel viewmodel;

    public OperationFragment() {
        // Required empty public constructor
    }


    public static OperationFragment newInstance() {
        fragment= fragment== null?new OperationFragment():fragment;
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_operation;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel=viewmodel==null?new AuditDetailViewmodel(getContext(),getSharedPre(),getBaseActivity()):viewmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        binding.callerAttachLay.questionsTxt.setText(R.string.caller_attach);
        binding.sleeveAttachLay.questionsTxt.setText(R.string.sleeve_attach);
        binding.callerAttachLay.noBtn.setText(R.string.fail);
        binding.callerAttachLay.yesBtn.setText(R.string.pass);
        binding.sleeveAttachLay.noBtn.setText(R.string.fail);
        binding.sleeveAttachLay.yesBtn.setText(R.string.pass);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
}