package com.kamakhya.dibzi.ui.auditDetails.survey2.approval;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseFragment;
import com.kamakhya.dibzi.databinding.FragmentApprovalBinding;
import com.kamakhya.dibzi.databinding.SelectLayCustomBinding;
import com.kamakhya.dibzi.ui.auditDetails.survey2.sampleDetail.adapterDialog.SelectionDilaogAdapter;
import com.kamakhya.dibzi.ui.auditDetails.viewmodel.AuditDetailViewmodel;

import java.util.ArrayList;
import java.util.List;

public class ApprovalFragment extends BaseFragment<FragmentApprovalBinding, AuditDetailViewmodel> {
    private FragmentApprovalBinding binding;
    private AuditDetailViewmodel viewmodel;
    private SelectLayCustomBinding searchViewBinding;
    private Dialog FailureReasonDialog;
    private static ApprovalFragment instance;
    private List<String>ApprovedSelectionList=new ArrayList<>();
    private SelectionDilaogAdapter dilaogAdapter;
    public ApprovalFragment() {
        // Required empty public constructor
    }

    public static ApprovalFragment newInstance() {
        return instance=instance==null?new ApprovalFragment():instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_approval;
    }

    @Override
    public AuditDetailViewmodel getViewModel() {
        return viewmodel = new AuditDetailViewmodel(getContext(), getSharedPre(), getBaseActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        DataAddedToList();
        searchViewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_lay_custom, null, false);
        DialogSelectReason();
       clickListner();
    }


    private void clickListner() {
        binding.selectReson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FailureReasonDialog.show();
            }
        });
        binding.selectReson2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FailureReasonDialog.show();
            }
        });
        binding.selectReson3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dilaogAdapter.UpdateList(ApprovedSelectionList);
                FailureReasonDialog.show();
            }
        });
        searchViewBinding.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchViewBinding.searchEdit.getVisibility() == View.VISIBLE) {
                    if (searchViewBinding.searchEdit.getText().toString().isEmpty()) {
                        searchViewBinding.searchEdit.setVisibility(View.GONE);
                        searchViewBinding.title.setVisibility(View.VISIBLE);
                    } else {
                        //do something here
                    }
                } else {
                    searchViewBinding.searchEdit.setVisibility(View.VISIBLE);
                    searchViewBinding.title.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }
    private void DialogSelectReason(){
        FailureReasonDialog= new Dialog(getContext(),android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        dilaogAdapter=new SelectionDilaogAdapter(getContext(),new ArrayList<>());
        FailureReasonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        FailureReasonDialog.setContentView(searchViewBinding.getRoot());
        searchViewBinding.selectionLayoutRecycler.setAdapter(dilaogAdapter);
        searchViewBinding.closeBtn.setOnClickListener(v -> {
            FailureReasonDialog.dismiss();
        });
        searchViewBinding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FailureReasonDialog.dismiss();
            }
        });
    }
    private void DataAddedToList() {
        ApprovedSelectionList = new ArrayList<>();
        ApprovedSelectionList.add(getResources().getString(R.string.construction));
        ApprovedSelectionList.add(getResources().getString(R.string.measurements));
        ApprovedSelectionList.add(getResources().getString(R.string.appearance));
        ApprovedSelectionList.add(getResources().getString(R.string.safety));
    }

}