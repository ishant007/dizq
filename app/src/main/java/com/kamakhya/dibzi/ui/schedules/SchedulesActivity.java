package com.kamakhya.dibzi.ui.schedules;
/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kamakhya.dibzi.R;
import com.kamakhya.dibzi.baseclasses.BaseActivity;
import com.kamakhya.dibzi.databinding.ActivitySchedulesBinding;
import com.kamakhya.dibzi.retrofitModel.getSchedule.ResultsItem;
import com.kamakhya.dibzi.setting.CommonUtils;
import com.kamakhya.dibzi.ui.auditDetails.AuditDetailsActivity;
import com.kamakhya.dibzi.ui.schedules.adapter.ScheduleAdapter;
import com.kamakhya.dibzi.ui.setting.activity.SettingActivity;
import com.kamakhya.dibzi.ui.updateschedule.UpdateScheduleActivity;

import java.util.Calendar;
import java.util.List;

import static com.kamakhya.dibzi.setting.AppConstance.SCHEDULE_BY_SHIPMENT;
import static com.kamakhya.dibzi.setting.AppConstance.VIEW_ALL_SHIPMENT;
import static com.kamakhya.dibzi.setting.AppConstance.VIEW_SHIPMENT_CUSTOM;

public class SchedulesActivity extends BaseActivity<ActivitySchedulesBinding, ScheduleViewModel> implements ScheduleNav {
    private ScheduleAdapter scheduleAdapter;
    private ActivitySchedulesBinding binding;
    private ScheduleViewModel viewModel;
    private Dialog internetDialog;
    private String yearMain, monthMain, dayMain;
    private int type = 0;
    private String ShipmentId;
    private Activity activity;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_schedules;
    }

    @Override
    public ScheduleViewModel getViewModel() {
        return viewModel = new ScheduleViewModel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        activity = this;
        type = getIntent().getIntExtra("type", 0);
        ShipmentId = getIntent().getStringExtra("ShipmentId");
        binding.toolbar.title.setText("Schedules");
        switch (type) {
            case SCHEDULE_BY_SHIPMENT: {
                if (ShipmentId != null && !ShipmentId.isEmpty()) {
                    viewModel.GetShcheduleByShipment(ShipmentId);
                } else {
                    showCustomAlert("No Shipment Id Found !!");
                    onBackPressed();
                }

                break;
            }
            case VIEW_ALL_SHIPMENT: {
                viewModel.GetShchedule();
                break;
            }
            case VIEW_SHIPMENT_CUSTOM: {
                break;
            }
        }
        scheduleAdapter = new ScheduleAdapter(this, this);
        binding.schedulerRecycler.setAdapter(scheduleAdapter);
        binding.toolbar.closeBtn.setImageResource(R.drawable.back);
        binding.toolbar.closeBtn.setVisibility(View.VISIBLE);

        binding.toolbar.dateIcon.setOnClickListener(v -> showDateTimePicker());
        binding.toolbar.dateIcon.setVisibility(View.VISIBLE);
        binding.toolbar.closeBtn.setOnClickListener(v -> onBackPressed());

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (internetDialog == null) {
            internetDialog = CommonUtils.InternetConnectionAlert(this, false);
        }
        if (isConnected) {
            internetDialog.dismiss();
        } else {
            internetDialog.show();
        }
    }

    @Override
    public void ClickOnEditButton(ResultsItem schedule, int position) {
        startActivity(new Intent(SchedulesActivity.this, UpdateScheduleActivity.class).putExtra("type",String.valueOf(schedule.getScheduleType())).putExtra("slno",String.valueOf(schedule.getSlNo())));
    }

    @Override
    public void ClickOnScheduleDetail(ResultsItem schedule, int position) {
        if (schedule.getScheduleType() != 0) {
            startActivity(new Intent(SchedulesActivity.this, AuditDetailsActivity.class).putExtra("schedule", new Gson().toJson(schedule, new TypeToken<ResultsItem>() {
            }.getType())));
        } else {
            showCustomAlert(getString(R.string.no_detail_found));
        }

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Logout() {

    }

    @Override
    public void showMessage(String message) {
        //
        binding.isEmptyTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void GetSchedules(List<ResultsItem> results) {
        binding.isEmptyTxt.setVisibility(View.GONE);
        scheduleAdapter.UpdateList(results);
    }

    @Override
    public void ClickOnDeleteScheduleDetail(ResultsItem schedule, int position) {
        viewModel.DeleteShchedule(String.valueOf(schedule.getSlNo()), position);
    }

    @Override
    public void DeleteSchedule(String returnMessage, int pos) {
        showCustomAlert(returnMessage);
        scheduleAdapter.removeFromList(pos);
    }


    public String showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                yearMain = String.valueOf(year);
                monthMain = String.valueOf(monthOfYear + 1);
                dayMain = String.valueOf(dayOfMonth);
                viewModel.GetShcheduleByDate(dayMain, monthMain, yearMain);
                binding.toolbar.title.setText(CommonUtils.getFormattedDate(SchedulesActivity.this, date.getTimeInMillis(), "dd MMM yyyy"));
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
        return CommonUtils.getFormattedDate(SchedulesActivity.this, date.getTimeInMillis());
    }
}