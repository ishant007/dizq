package com.kamakhya.dibzi.retrofitModel.getDataByShipmentId;

import com.google.gson.annotations.SerializedName;

public class Result{

	@SerializedName("buyerPoNo")
	private String buyerPoNo;

	@SerializedName("factory")
	private String factory;

	@SerializedName("auditor")
	private Object auditor;

	@SerializedName("auditType")
	private Object auditType;

	@SerializedName("slNo")
	private int slNo;

	@SerializedName("productCategory")
	private String productCategory;

	@SerializedName("companyId")
	private int companyId;

	@SerializedName("shipmentId")
	private Object shipmentId;

	@SerializedName("supplier")
	private String supplier;

	@SerializedName("scheduleDate")
	private Object scheduleDate;

	@SerializedName("style")
	private String style;

	@SerializedName("poSlNo")
	private int poSlNo;

	@SerializedName("offeredQuantity")
	private double offeredQuantity;

	@SerializedName("poNumber")
	private String poNumber;

	public void setBuyerPoNo(String buyerPoNo){
		this.buyerPoNo = buyerPoNo;
	}

	public String getBuyerPoNo(){
		return buyerPoNo;
	}

	public void setFactory(String factory){
		this.factory = factory;
	}

	public String getFactory(){
		return factory;
	}

	public void setAuditor(Object auditor){
		this.auditor = auditor;
	}

	public Object getAuditor(){
		return auditor;
	}

	public void setAuditType(Object auditType){
		this.auditType = auditType;
	}

	public Object getAuditType(){
		return auditType;
	}

	public void setSlNo(int slNo){
		this.slNo = slNo;
	}

	public int getSlNo(){
		return slNo;
	}

	public void setProductCategory(String productCategory){
		this.productCategory = productCategory;
	}

	public String getProductCategory(){
		return productCategory;
	}

	public void setCompanyId(int companyId){
		this.companyId = companyId;
	}

	public int getCompanyId(){
		return companyId;
	}

	public void setShipmentId(Object shipmentId){
		this.shipmentId = shipmentId;
	}

	public Object getShipmentId(){
		return shipmentId;
	}

	public void setSupplier(String supplier){
		this.supplier = supplier;
	}

	public String getSupplier(){
		return supplier;
	}

	public void setScheduleDate(Object scheduleDate){
		this.scheduleDate = scheduleDate;
	}

	public Object getScheduleDate(){
		return scheduleDate;
	}

	public void setStyle(String style){
		this.style = style;
	}

	public String getStyle(){
		return style;
	}

	public void setPoSlNo(int poSlNo){
		this.poSlNo = poSlNo;
	}

	public int getPoSlNo(){
		return poSlNo;
	}

	public void setOfferedQuantity(double offeredQuantity){
		this.offeredQuantity = offeredQuantity;
	}

	public double getOfferedQuantity(){
		return offeredQuantity;
	}

	public void setPoNumber(String poNumber){
		this.poNumber = poNumber;
	}

	public String getPoNumber(){
		return poNumber;
	}
}