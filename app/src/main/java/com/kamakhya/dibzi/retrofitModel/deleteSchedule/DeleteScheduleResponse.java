package com.kamakhya.dibzi.retrofitModel.deleteSchedule;

import com.google.gson.annotations.SerializedName;

public class DeleteScheduleResponse{

	@SerializedName("results")
	private Results results;

	public void setResults(Results results){
		this.results = results;
	}

	public Results getResults(){
		return results;
	}
}