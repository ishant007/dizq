package com.kamakhya.dibzi.retrofitModel.login;

import com.google.gson.annotations.SerializedName;

public class GroupNameListItem{

	@SerializedName("groupName")
	private String groupName;

	@SerializedName("role_Modules_Permission_Model_List")
	private Object roleModulesPermissionModelList;

	public void setGroupName(String groupName){
		this.groupName = groupName;
	}

	public String getGroupName(){
		return groupName;
	}

	public void setRoleModulesPermissionModelList(Object roleModulesPermissionModelList){
		this.roleModulesPermissionModelList = roleModulesPermissionModelList;
	}

	public Object getRoleModulesPermissionModelList(){
		return roleModulesPermissionModelList;
	}

	@Override
 	public String toString(){
		return 
			"GroupNameListItem{" + 
			"groupName = '" + groupName + '\'' + 
			",role_Modules_Permission_Model_List = '" + roleModulesPermissionModelList + '\'' + 
			"}";
		}
}