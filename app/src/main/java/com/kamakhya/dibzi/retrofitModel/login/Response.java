package com.kamakhya.dibzi.retrofitModel.login;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("results")
	private Results results;

	public void setResults(Results results){
		this.results = results;
	}

	public Results getResults(){
		return results;
	}

	@Override
 	public String toString(){
		return new Gson().toJson(this);
	}
}