package com.kamakhya.dibzi.retrofitModel.getDataByShipmentId;

import com.google.gson.annotations.SerializedName;

public class ShipmentIdDataResponse{

	@SerializedName("result")
	private Result result;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}
}