package com.kamakhya.dibzi.retrofitModel.createSchedule;

import com.google.gson.annotations.SerializedName;

public class CreateScheduleResponse{

	@SerializedName("results")
	private Results results;

	public void setResults(Results results){
		this.results = results;
	}

	public Results getResults(){
		return results;
	}

	@Override
 	public String toString(){
		return 
			"CreateScheduleResponse{" + 
			"results = '" + results + '\'' + 
			"}";
		}
}