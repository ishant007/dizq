package com.kamakhya.dibzi.retrofitModel.getPoDetails;

import com.google.gson.annotations.SerializedName;

public class Result {

	@SerializedName("factory")
	private String factory;

	@SerializedName("orderedQuantity")
	private double orderedQuantity;

	@SerializedName("garmentVendor")
	private String garmentVendor;

	@SerializedName("auditor")
	private String auditor;

	@SerializedName("description")
	private String description;

	@SerializedName("auditType")
	private String auditType;

	@SerializedName("styleCode")
	private String styleCode;

	@SerializedName("slNo")
	private int slNo;

	@SerializedName("productCategory")
	private String productCategory;

	@SerializedName("companyId")
	private int companyId;

	@SerializedName("scheduleDate")
	private String scheduleDate;

	@SerializedName("poSlNo")
	private int poSlNo;

	@SerializedName("poNumber")
	private String poNumber;

	@SerializedName("customer")
	private String customer;

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public double getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(double orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public String getGarmentVendor() {
		return garmentVendor;
	}

	public void setGarmentVendor(String garmentVendor) {
		this.garmentVendor = garmentVendor;
	}

	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public String getStyleCode() {
		return styleCode;
	}

	public void setStyleCode(String styleCode) {
		this.styleCode = styleCode;
	}

	public int getSlNo() {
		return slNo;
	}

	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public int getPoSlNo() {
		return poSlNo;
	}

	public void setPoSlNo(int poSlNo) {
		this.poSlNo = poSlNo;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
}