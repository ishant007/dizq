package com.kamakhya.dibzi.retrofitModel.getPoNumber;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetPoNumberResponse{

	@SerializedName("results")
	private List<ResultsItem> results;

	public void setResults(List<ResultsItem> results){
		this.results = results;
	}

	public List<ResultsItem> getResults(){
		return results;
	}
}