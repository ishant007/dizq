package com.kamakhya.dibzi.retrofitModel.createSchedule;

import com.google.gson.annotations.SerializedName;

public class Results{

	@SerializedName("returnStatus")
	private String returnStatus;

	@SerializedName("returnMessage")
	private String returnMessage;

	@SerializedName("id")
	private int id;

	public void setReturnStatus(String returnStatus){
		this.returnStatus = returnStatus;
	}

	public String getReturnStatus(){
		return returnStatus;
	}

	public void setReturnMessage(String returnMessage){
		this.returnMessage = returnMessage;
	}

	public String getReturnMessage(){
		return returnMessage;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Results{" + 
			"returnStatus = '" + returnStatus + '\'' + 
			",returnMessage = '" + returnMessage + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}