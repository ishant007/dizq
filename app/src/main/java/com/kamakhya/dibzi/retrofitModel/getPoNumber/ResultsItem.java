package com.kamakhya.dibzi.retrofitModel.getPoNumber;

import com.google.gson.annotations.SerializedName;

public class ResultsItem{

	@SerializedName("disabled")
	private boolean disabled;

	@SerializedName("text")
	private String PONumber;

	@SerializedName("value")
	private String POSLNumber;

	@SerializedName("selected")
	private boolean selected;

	@SerializedName("group")
	private String group;

	public void setDisabled(boolean disabled){
		this.disabled = disabled;
	}

	public boolean isDisabled(){
		return disabled;
	}

	public String getPONumber() {
		return PONumber;
	}

	public void setPONumber(String PONumber) {
		this.PONumber = PONumber;
	}

	public String getPOSLNumber() {
		return POSLNumber;
	}

	public void setPOSLNumber(String POSLNumber) {
		this.POSLNumber = POSLNumber;
	}

	public void setSelected(boolean selected){
		this.selected = selected;
	}

	public boolean isSelected(){
		return selected;
	}

	public void setGroup(String group){
		this.group = group;
	}

	public Object getGroup(){
		return group;
	}
}