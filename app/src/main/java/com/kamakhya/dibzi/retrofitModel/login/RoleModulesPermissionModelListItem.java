package com.kamakhya.dibzi.retrofitModel.login;

import com.google.gson.annotations.SerializedName;

public class RoleModulesPermissionModelListItem{

	@SerializedName("isView")
	private boolean isView;

	@SerializedName("roleID")
	private int roleID;

	@SerializedName("isDelete")
	private boolean isDelete;

	@SerializedName("displayName")
	private Object displayName;

	@SerializedName("isRead")
	private boolean isRead;

	@SerializedName("moduleName")
	private String moduleName;

	@SerializedName("isRight")
	private boolean isRight;

	@SerializedName("groupName")
	private String groupName;

	@SerializedName("isEdit")
	private boolean isEdit;

	@SerializedName("formName")
	private String formName;

	@SerializedName("isSelected")
	private boolean isSelected;

	@SerializedName("id")
	private int id;

	@SerializedName("moduleID")
	private int moduleID;

	@SerializedName("isWrite")
	private boolean isWrite;

	public void setIsView(boolean isView){
		this.isView = isView;
	}

	public boolean isIsView(){
		return isView;
	}

	public void setRoleID(int roleID){
		this.roleID = roleID;
	}

	public int getRoleID(){
		return roleID;
	}

	public void setIsDelete(boolean isDelete){
		this.isDelete = isDelete;
	}

	public boolean isIsDelete(){
		return isDelete;
	}

	public void setDisplayName(Object displayName){
		this.displayName = displayName;
	}

	public Object getDisplayName(){
		return displayName;
	}

	public void setIsRead(boolean isRead){
		this.isRead = isRead;
	}

	public boolean isIsRead(){
		return isRead;
	}

	public void setModuleName(String moduleName){
		this.moduleName = moduleName;
	}

	public String getModuleName(){
		return moduleName;
	}

	public void setIsRight(boolean isRight){
		this.isRight = isRight;
	}

	public boolean isIsRight(){
		return isRight;
	}

	public void setGroupName(String groupName){
		this.groupName = groupName;
	}

	public String getGroupName(){
		return groupName;
	}

	public void setIsEdit(boolean isEdit){
		this.isEdit = isEdit;
	}

	public boolean isIsEdit(){
		return isEdit;
	}

	public void setFormName(String formName){
		this.formName = formName;
	}

	public String getFormName(){
		return formName;
	}

	public void setIsSelected(boolean isSelected){
		this.isSelected = isSelected;
	}

	public boolean isIsSelected(){
		return isSelected;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setModuleID(int moduleID){
		this.moduleID = moduleID;
	}

	public int getModuleID(){
		return moduleID;
	}

	public void setIsWrite(boolean isWrite){
		this.isWrite = isWrite;
	}

	public boolean isIsWrite(){
		return isWrite;
	}

	@Override
 	public String toString(){
		return 
			"RoleModulesPermissionModelListItem{" + 
			"isView = '" + isView + '\'' + 
			",roleID = '" + roleID + '\'' + 
			",isDelete = '" + isDelete + '\'' + 
			",displayName = '" + displayName + '\'' + 
			",isRead = '" + isRead + '\'' + 
			",moduleName = '" + moduleName + '\'' + 
			",isRight = '" + isRight + '\'' + 
			",groupName = '" + groupName + '\'' + 
			",isEdit = '" + isEdit + '\'' + 
			",formName = '" + formName + '\'' + 
			",isSelected = '" + isSelected + '\'' + 
			",id = '" + id + '\'' + 
			",moduleID = '" + moduleID + '\'' + 
			",isWrite = '" + isWrite + '\'' + 
			"}";
		}
}