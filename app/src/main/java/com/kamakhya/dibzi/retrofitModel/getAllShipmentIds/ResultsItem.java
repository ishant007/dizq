package com.kamakhya.dibzi.retrofitModel.getAllShipmentIds;

import com.google.gson.annotations.SerializedName;

public class ResultsItem{

	@SerializedName("disabled")
	private boolean disabled;

	@SerializedName("text")
	private String text;

	@SerializedName("value")
	private String value;

	@SerializedName("selected")
	private boolean selected;

	@SerializedName("group")
	private Object group;

	public void setDisabled(boolean disabled){
		this.disabled = disabled;
	}

	public boolean isDisabled(){
		return disabled;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setSelected(boolean selected){
		this.selected = selected;
	}

	public boolean isSelected(){
		return selected;
	}

	public void setGroup(Object group){
		this.group = group;
	}

	public Object getGroup(){
		return group;
	}

	@Override
 	public String toString(){
		return 
			"ResultsItem{" + 
			"disabled = '" + disabled + '\'' + 
			",text = '" + text + '\'' + 
			",value = '" + value + '\'' + 
			",selected = '" + selected + '\'' + 
			",group = '" + group + '\'' + 
			"}";
		}
}