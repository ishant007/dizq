package com.kamakhya.dibzi.retrofitModel.getPoDetails;

import com.google.gson.annotations.SerializedName;

public class GetPoDetailsResponse{

	@SerializedName("result")
	private Result result;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}
}