package com.kamakhya.dibzi.retrofitModel.getSchedule;

import com.google.gson.annotations.SerializedName;

public class ResultsItem{

	@SerializedName("date")
	private String date;

	@SerializedName("auditor")
	private String auditor;

	@SerializedName("factoryName")
	private String factoryName;

	@SerializedName("inspectionType")
	private String inspectionType;

	@SerializedName("slNo")
	private int slNo;

	private int scheduleType;

	public int getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(int scheduleType) {
		this.scheduleType = scheduleType;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setAuditor(String auditor){
		this.auditor = auditor;
	}

	public String getAuditor(){
		return auditor;
	}

	public void setFactoryName(String factoryName){
		this.factoryName = factoryName;
	}

	public String getFactoryName(){
		return factoryName;
	}

	public void setInspectionType(String inspectionType){
		this.inspectionType = inspectionType;
	}

	public String getInspectionType(){
		return inspectionType;
	}

	public void setSlNo(int slNo){
		this.slNo = slNo;
	}

	public int getSlNo(){
		return slNo;
	}
}