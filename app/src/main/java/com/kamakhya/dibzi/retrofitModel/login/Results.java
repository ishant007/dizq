package com.kamakhya.dibzi.retrofitModel.login;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class Results{

	@SerializedName("userSubCompId")
	private int userSubCompId;

	@SerializedName("employeeName")
	private String employeeName;

	@SerializedName("groupNameList")
	private List<GroupNameListItem> groupNameList;

	@SerializedName("empId")
	private String empId;

	@SerializedName("userDomainId")
	private int userDomainId;

	@SerializedName("companyName")
	private Object companyName;

	@SerializedName("loginStatus")
	private String loginStatus;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userId")
	private int userId;

	@SerializedName("poZone")
	private Object poZone;

	@SerializedName("token")
	private String token;

	@SerializedName("userDivision")
	private String userDivision;

	@SerializedName("companyId")
	private String companyId;

	@SerializedName("userParentCompId")
	private int userParentCompId;

	@SerializedName("userImage")
	private String userImage;

	@SerializedName("userType_1")
	private Object userType1;

	@SerializedName("theme")
	private String theme;

	@SerializedName("userEmail")
	private String userEmail;

	@SerializedName("userType")
	private String userType;

	@SerializedName("userRoleId")
	private int userRoleId;

	@SerializedName("userRole")
	private String userRole;

	@SerializedName("supplierType")
	private Object supplierType;

	@SerializedName("role_Modules_Permission_Model_List")
	private List<RoleModulesPermissionModelListItem> roleModulesPermissionModelList;

	public void setUserSubCompId(int userSubCompId){
		this.userSubCompId = userSubCompId;
	}

	public int getUserSubCompId(){
		return userSubCompId;
	}

	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}

	public String getEmployeeName(){
		return employeeName;
	}

	public void setGroupNameList(List<GroupNameListItem> groupNameList){
		this.groupNameList = groupNameList;
	}

	public List<GroupNameListItem> getGroupNameList(){
		return groupNameList;
	}

	public void setEmpId(String empId){
		this.empId = empId;
	}

	public String getEmpId(){
		return empId;
	}

	public void setUserDomainId(int userDomainId){
		this.userDomainId = userDomainId;
	}

	public int getUserDomainId(){
		return userDomainId;
	}

	public void setCompanyName(Object companyName){
		this.companyName = companyName;
	}

	public Object getCompanyName(){
		return companyName;
	}

	public void setLoginStatus(String loginStatus){
		this.loginStatus = loginStatus;
	}

	public String getLoginStatus(){
		return loginStatus;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setPoZone(Object poZone){
		this.poZone = poZone;
	}

	public Object getPoZone(){
		return poZone;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setUserDivision(String userDivision){
		this.userDivision = userDivision;
	}

	public String getUserDivision(){
		return userDivision;
	}

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setUserParentCompId(int userParentCompId){
		this.userParentCompId = userParentCompId;
	}

	public int getUserParentCompId(){
		return userParentCompId;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setUserType1(Object userType1){
		this.userType1 = userType1;
	}

	public Object getUserType1(){
		return userType1;
	}

	public void setTheme(String theme){
		this.theme = theme;
	}

	public String getTheme(){
		return theme;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUserRoleId(int userRoleId){
		this.userRoleId = userRoleId;
	}

	public int getUserRoleId(){
		return userRoleId;
	}

	public void setUserRole(String userRole){
		this.userRole = userRole;
	}

	public String getUserRole(){
		return userRole;
	}

	public void setSupplierType(Object supplierType){
		this.supplierType = supplierType;
	}

	public Object getSupplierType(){
		return supplierType;
	}

	public void setRoleModulesPermissionModelList(List<RoleModulesPermissionModelListItem> roleModulesPermissionModelList){
		this.roleModulesPermissionModelList = roleModulesPermissionModelList;
	}

	public List<RoleModulesPermissionModelListItem> getRoleModulesPermissionModelList(){
		return roleModulesPermissionModelList;
	}

	@Override
 	public String toString(){
		return new Gson().toJson(this);
		}
}